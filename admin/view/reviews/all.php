<?php
if(isset($_GET['city'])){
    $rootPath = '/' . $_GET['city'];
} else {
    $rootPath = '/all';
}
?>
<h2 class="h2title">Все отзывы</h2>
<div class="row">
    <?php foreach($this->data['reviews'] as $reviews):?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="thumbnail" style="padding: 15px">
                <div class="row">
                    <div class="col-xs-4 col-sm-3">
                        <a href="<?php echo $rootPath?>/reviews/view/<?php echo $reviews->id?>"><img src="/img/textreviews/<?php echo $reviews->urlPhoto?>" alt="<?php echo $reviews->title?>" class="img-thumbnail" width="100%"></a>
                    </div>
                    <div class="col-xs-8 col-sm-9">
                        <div><a href="<?php echo $rootPath?>/reviews/view/<?php echo $reviews->id?>"><span style="font-size:18px"><?php echo $reviews->title?></span></a></div>
                        <div><b><?php echo $reviews->dateTime?></b></div>
                        <p><?php echo $reviews->longTextReviews?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><a href="<?php echo $rootPath?>/reviews/view/<?php echo $reviews->id?>" class="btn btn-default text-right" style="float:right">Читать полностью</a></div>
                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>
