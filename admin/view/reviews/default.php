<div class="row">
    <div class="col-xs-12">
        <h2 class="underline-header">Отзывы</h2>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Пользователь</th>
                    <th>Дата</th>
                    <th>Текст</th>
                    <th>Статус</th>
                    <th>Опции</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($this->data['reviews'] as $review){?>
                    <tr id="itemReview<?php echo $review->id?>">
                        <td><?php echo $review->id?></td>
                        <td><?php echo $review->userName?></td>
                        <td><?php echo $review->date?></td>
                        <td><?php echo $review->text?></td>
                        <td>
                            <?php if($review->isPublic){
                                echo '<span class="label label-success" id="statusReview'.$review->id.'">Опубликован</span>';
                            } else{
                                echo '<span class="label label-danger" id="statusReview'.$review->id.'">Не опубликован</span>';
                            }?>
                        </td>
                        <td>
                            <?php if($review->isPublic){?>
                                <button type="button" class="btn btn-primary" title="Снять с публикации" id="optionUnPublic<?php echo $review->id?>" onclick="adminReview.unPublish(<?php echo $review->id ?>)"><span class="glyphicon glyphicon-minus"></span></button>
                                <button type="button" class="btn btn-danger" title="Удалять" id="optionDel<?php echo $review->id?>" onclick="adminReview.removeItem(<?php echo $review->id ?>)"><span class="glyphicon glyphicon-remove"></span></button>
                            <?php } else {?>
                                <button type="button" class="btn btn-success" title="Опубликовать" id="optionPublic<?php echo $review->id?>" onclick="adminReview.publish(<?php echo $review->id ?>)"><span class="glyphicon glyphicon-ok"></span></button>
                                <button type="button" class="btn btn-danger" title="Удалять" id="optionDel<?php echo $review->id?>" onclick="adminReview.removeItem(<?php echo $review->id ?>)"><span class="glyphicon glyphicon-remove"></span></button>
                            <?php }?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h2 class="underline-header">Оставить отзыв</h2>
        <div class="row">
            <div class="form col-xs-6">
                <div id="alert-review" class="alert" style="display: none"></div>
                <div class="form-group">
                    <label for="reviewName">Ваше имя</label>
                    <input type="text" class="form-control" id="reviewName" placeholder="Иван">
                </div>
                <div class="form-group">
                    <label for="reviewText" class="control-label">Отзыв</label>
                    <textarea class="form-control" id="reviewText" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default" onclick="Reviews.send()">Отправить</button>
                </div>
            </div>
        </div>
    </div>
</div>