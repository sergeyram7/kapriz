<?php

namespace App\Controller;
use App\Model\Producer as ProducerModel;
use App\Model\Collection;
use App\Model\Product;
use App\Model\Category;
use App\View\View;

class Producer{
//    public function action_all($where=[]){
//        if(empty($where)) $where=[];
//        $category = Category::getAll();
//        $view = new View();
//        $view->assign('category',$category );
//        $view->assign('pageTitle', 'Категории :: ');
//        $view->assign('display', 'category/list.php');
//        $view->template();
//    }
    public function action_view(){
        if ($_GET['alias']) {
            //$category = CategoryModel::getSome(1, ['alias' => $_GET['alias']], []);
            $producer = ProducerModel::getSome(1,['alias' => $_GET['alias']]);
            $collection = Collection::getAll(['producer_id' => $producer[0]->id]);
            for($i=0;isset($collection[$i]);$i++){
                $countProduct[$i] = Product::getNumAll(['collection_id'=>$collection[$i]->id]);
            }
            for($i=0;isset($collection[$i]);$i++){
                $category[$i] = Category::getSome(1,['id'=>$collection[$i]->category_id],[])[0];
            }
            $view = new View();
            $view->assign('producer', $producer);
            $view->assign('collection', $collection);
            $view->assign('countProduct',$countProduct);
            $view->assign('category',$category);
            $view->assign('pageTitle', 'Коллекции керамической плитки от ' . $producer[0]->name . ' :: ');
            $view->assign('display', 'producer/default.php');
            $view->template();
        } else {
            header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
            include_once __DIR__ .'/../404.html';
        }
    }
}