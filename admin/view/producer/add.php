<?php //$item = $this->data['producer'][0];
//if(!empty($this->data['save'])){
//    echo '<div class="alert alert-success"><strong>Сохранено</strong></div>';
//}
//?>
<div class="row">
    <div class="col-md-12">
        <h2>Добавить производителя</h2>
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Имя:</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="alias">Алиас (формируется сам, после имени):</label>
                <input type="text" class="form-control" id="alias" name="alias">
            </div>

            <div class="form-group">
                <label for="summernote">Описание:</label>
                <textarea class="form-control" id="summernote" name="description" rows="8"></textarea>
            </div>
            <div class="checkbox">
                <?php foreach($this->data['category'] as $category){?>
                    <label>
                        <input type="checkbox" value="<?php echo $category->id?>" name="category_id[]"> <?php echo $category->name?>
                    </label>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="img">Логотип производителя</label>
                <input type="file" id="img" name="img">
            </div>
            <div class="form-group">
                <label for="metaKeywords">Ключевые слова:</label>
                <input type="text" class="form-control" id="metaKeywords" name="metaKeywords">
            </div>
            <div class="form-group">
                <label for="metaDescription">Description:</label>
                <input type="text" class="form-control" id="metaDescription" name="metaDescription">
            </div>
            <button type="submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
</div>
