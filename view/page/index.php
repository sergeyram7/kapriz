<?php
$page = $this->data['page'][0];
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="underline-header"><?php echo $page->name?></h1>
        <?php echo $page->text?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <h2 class="underline-header">Рекомендованные коллекции</h2>
        <div class="row">
            <?php
                foreach($this->data['collection'] as $key => $collection){?>
                    <div class="col-md-3">
                        <div class="thumbnail">
                            <div class="wrap-img"><a href="/category/view/<?php echo $this->data['category'][$key]->alias . '/' . $this->data['producer'][$key]->alias . '/' . $collection->id . '/' . $collection->alias?>"><img src="/img/collection/min/<?php echo $collection->img?>" alt="<?php echo $collection->name?>"></a></div>
                            <div class="caption">
                                <h3><a href="/category/view/<?php echo $this->data['category'][$key]->alias . '/' . $this->data['producer'][$key]->alias . '/' . $collection->id . '/' . $collection->alias?>"><?php echo $collection->name?></a></h3>
                                <p class="short_description"><?php echo $collection->short_description?></p>
                                <ul>
                                    <li><strong>Позиций в коллекции: </strong><?php echo($this->data['countProduct'][$key])?></li>
                                </ul>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a href="/producer/view/<?php echo($this->data['producer'][$key]->alias)?>" class="btn btn-link btn-producer" role="button" title="Посмотреть все коллекции от '<?php echo($this->data['producer'][$key]->name)?>'"><?php echo $this->data['producer'][$key]->name;/*echo mb_substr($this->data['producer'][$key]->name,0,13); echo mb_strlen($this->data['producer'][$key]->name)>13?'..':''*/?></a>
                                    </div>
                                    <div class="col-xs-12">
                                        <a href="/category/view/<?php echo $this->data['category'][$key]->alias . '/' . $this->data['producer'][$key]->alias . '/' . $collection->id . '/' . $collection->alias?>" class="btn btn-danger" role="button">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php } ?>
        </div>
    </div>
</div>
