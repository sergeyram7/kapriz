<?php

namespace Admin\View;

class View
{
    protected $data = [];
    protected $rootPath = '';
    public function __construct(){
        if(isset($_GET['city'])){
            $this->rootPath = '/' . $_GET['city'];
        } else {
            $this->rootPath = '/all';
        }
    }
    public function assign($name,$value){
        $this->data[$name] = $value;
    }
    public function template(){
            require_once __DIR__ . '/../view/admin.php';
    }
    public function display($template){
        if (file_exists(__DIR__ . '/../view/' . $template)) {
            include __DIR__ . '/../view/' . $template;
        }
    }
}