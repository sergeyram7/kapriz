<?php

namespace Admin\Controller;
use Admin\Model\Category;
use Admin\Model\Orders;
use Admin\Model\OrderProduct;
use Admin\Model\Collection;
use Admin\Model\Product;
use Admin\Model\Producer;
use Admin\View\View;

class Order
{
    public function action_all(){
        $orders = Orders::getAll([],['id'=>'DESC']);
        $view = new View();
        $view->assign('orders',$orders);
        $view->assign('pageTitle', 'Главная страница :: ');
        $view->assign('display', 'order/default.php');
        $view->template();
    }

    public function action_view(){
        $orderProduct = OrderProduct::getAll(['order_id'=>$_GET['id']]);
        $product = [];
        $collection = [];
        for ($i=0;!empty($orderProduct[$i]);$i++) {
            $product[$i] = Product::getSome(1, ['id' => $orderProduct[$i]->product_id])[0];
            $collection[$i] = Collection::getSome(1, ['id' => $product[$i]->collection_id])[0];
        }

//        $orders = Orders::getAll([],['id'=>'DESC']);
        $view = new View();
//        $view->assign('orders',$orders);

        $view->assign('collection',$collection);
        $view->assign('product',$product);
        $view->assign('orderProduct',$orderProduct);
        $view->assign('pageTitle', 'Просмотр заказа :: ');
        $view->assign('display', 'order/view.php');
        $view->template();
    }
}