<?php

use App\Controller\Index;

$ctrl = isset($_GET['ctrl'])?'App\Controller\\'.$_GET['ctrl']:'App\Controller\Index';

$controller = new $ctrl;

//$activeSlider = \App\Model\Slider::getAll()[0]->active;
$str = file_get_contents(__DIR__ . '/../setting.json');
$activeSlider = json_decode($str, true);
//$activeSlider = \App\Model\Slider::getAll()[0]->active;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->data['pageTitle']?></title>
    <meta name="keywords" content="<?php if (!empty($this->data['keywords'])) {
        echo $this->data['keywords'];
    } else {
        echo ' ';
    }?>">
    <meta name="description" content="<?php if (!empty($this->data['description'])) {
        echo $this->data['description'];
    } else {
        echo ' ';
    }?>">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <!-- подключение CSS файла Fancybox -->
    <link rel="stylesheet" href="/css/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <meta name="yandex-verification" content="d8bb843a65435d84" />
    <meta name="google-site-verification" content="pJpMA-cK1O5npP0b1wqeeurjKZMUQ_lrBItWdtm1Bfw" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Разметка JSON-LD, созданная Мастером разметки структурированных данных Google. -->
    <script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "LocalBusiness",
  "name" : "Керамическая плитка в Саратове. Салон магазин «Каприз».",
  "image" : "http://kapriz-keramik.ru/img/Kapriz_salon_magazin800_1.png",
  "telephone" : "+7(8452) 64-93-45 +7(927) 119-30-30",
  "email" : "kapriz-keramik@yandex.ru",
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "ул. Университетская 109",
    "addressLocality" : "г. Саратов",
    "addressRegion" : "Саратовская область",
    "addressCountry" : "Россия",
    "postalCode" : "410005"
  },
  "url" : "http://kapriz-keramik.ru/"
}
</script>
</head>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38690790 = new Ya.Metrika({
                    id:38690790,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38690790" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
    var _tmr = window._tmr || (window._tmr = []);
    _tmr.push({id: "2802181", type: "pageView", start: (new Date()).getTime()});
    (function (d, w, id) {
        if (d.getElementById(id)) return;
        var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
        ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
        var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
        if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
    })(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
        <img src="//top-fwz1.mail.ru/counter?id=2802181;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
    </div></noscript>
<!-- //Rating@Mail.ru counter -->

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57cd89d802d025e1"></script>-->
<!-- uSocial -->
<script async src="https://usocial.pro/usocial/usocial.js?v=6.1.1" data-script="usocial" charset="utf-8"></script>
<div class="uSocial-Share" data-pid="ae7453b692eba56b0d26a144b33cefb4" data-type="share" data-options="round-rect,style1,left,slide-down,upArrow-right,size32,eachCounter0,counter0" data-social="vk,fb,twi,ok,gPlus,lj,mail,spoiler,pinterest" data-mobile="vi,wa,telegram,sms"></div><!-- /uSocial -->
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5BN2D4"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5BN2D4');</script>
<!-- End Google Tag Manager -->

<header class="header">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-xs visible-sm" href="#" style="color: #990000">КАПРИЗ - КЕРАМИК</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/">О компании <span class="sr-only">(current)</span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Каталог <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/category/view/plitka_dlya_vannoy">Плитка для ванной</a></li>
                            <li><a href="/category/view/plitka_dlya_kukhni_v_saratove">Плитка для кухни</a></li>
                            <li><a href="/category/view/plitka_dlya_pola_v_saratove">Плитка для пола</a></li>
                            <li><a href="/category/view/plitka_mozaika_v_saratove">Плитка мозаика</a></li>
                            <li><a href="/category/view/keramogranit_v_saratove">Плитка керамогранит</a></li>
                            <li><a href="/category/view/oblitsovochnyy_kamen_v_saratove">Облицовочный камень</a></li>
                            <li><a href="/category/view/plitka_klinker_v_saratove">Клинкер</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/category/view/premium"><img src="/img/ico/premium.png" title="Премиум товары" alt="Премиум" style="margin-top: -4px;margin-right: 4px">Премиум</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Покупателям <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/index/page/shares">Скидки</a></li>
                            <li><a href="/category/view/consumables/other/108/soputstvuyshie_tovary">Сопутствующие товары</a></li>
                            <li><a href="/index/page/payment">Доставка</a></li>
                            <li><a href="/index/page/uslugi_otdelochnikov">Услуги отделочников</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/index/page/optom">Оптом</a></li>
<!--                            <li><a href="/index/page/partneram">Партнерам</a></li>-->
                        </ul>
                    </li>
                    <li><a href="/index/page/contacts">Контакты</a></li>
                    <!--<li><a href="#">Оплата и доставка</a></li>-->
                    <li><a href="/reviews">Отзывы</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="visible-md visible-lg wall">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-lg-4" style="height: 600px">
                    <div class="column">
                        <a href="/"><img src="/img/logo.png" alt="КАПРИЗ - КЕРАМИК" class="logo"></a>
                        <article class="feedback text-center">
                                <span style="font-size: 18pt;color:#fff">Интересует вопрос?</span>
                                <p><span>Закажите</span> <br><small>обратный звонок</small></p>
                                <div class="col-md-9" style="margin-left: 40px; padding-bottom: 33px">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="callme-phone" maxlength="19" placeholder="Ваш номер?">
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" type="button" onclick="CallMe.validator()">Заказать</button>
                                        </span>
                                    </div><!-- /input-group -->
                                </div>
                                <p>Приходите в наш салон <br><small>г. Саратов, ул. Университетская 109 <br>(угол Соколовой)</small></p>
                            <div style="margin-top: 30px;font-size: 20pt; color: #fff;font-weight: 600;text-shadow: 4px 4px 2px rgb(70, 70, 70);"><span class="glyphicon glyphicon-phone-alt"></span> 8 (8452) 64-93-45</div>
                            <div style="margin-top: -5px;font-size: 20pt; color: #fff;font-weight: 600;text-shadow: 4px 4px 2px rgb(70, 70, 70);"><span class="glyphicon glyphicon-phone"></span> 8 927 119-30-30</div>
                        </article>
                    </div>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div id="carousel-example-generic" class="carousel slide effect6" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" <?php if($activeSlider['slider']['select']===0) echo 'class="active"'?>></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1" <?php if($activeSlider['slider']['select']==1) echo 'class="active"'?>></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2" <?php if($activeSlider['slider']['select']==2) echo 'class="active"'?>></li>
                            <li data-target="#carousel-example-generic" data-slide-to="3" <?php if($activeSlider['slider']['select']==3) echo 'class="active"'?>></li>
                            <li data-target="#carousel-example-generic" data-slide-to="4" <?php if($activeSlider['slider']['select']==4) echo 'class="active"'?>></li>
                            <li data-target="#carousel-example-generic" data-slide-to="5" <?php if($activeSlider['slider']['select']==5) echo 'class="active"'?>></li>
                            <li data-target="#carousel-example-generic" data-slide-to="6" <?php if($activeSlider['slider']['select']==6) echo 'class="active"'?>></li>
                            <li data-target="#carousel-example-generic" data-slide-to="7" <?php if($activeSlider['slider']['select']==7) echo 'class="active"'?>></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item <?php if($activeSlider['slider']['select']===0) echo 'active'?>">
                                <img src="/img/slider/uralkeramika.jpg" alt="Уралкерамика">
                                <div class="carousel-caption">
                                    <span style="font-size: 24pt;text-transform: uppercase">Уралкерамика</span>
                                    <p>Широкий ассортимент выпускаемой продукции</p>
                                </div>
                            </div>
                            <div class="item <?php if($activeSlider['slider']['select']==1) echo 'active'?>">
                                <img src="/img/slider/berezakeramika.jpg" alt="Березакерамика">
                                <div class="carousel-caption">
                                    <span style="font-size: 24pt;text-transform: uppercase">Березакерамика</span>
                                    <p>«Березакерамика» производится на высокотехнологичной итальянской линии "SACMI"</p>
                                </div>
                            </div>
                            <div class="item <?php if($activeSlider['slider']['select']==2) echo 'active'?>">
                                <img src="/img/slider/shahtinskaya.jpg" alt="ШАХТИНСКАЯ ПЛИТКА">
                                <div class="carousel-caption">
                                    <span style="font-size: 24pt;text-transform: uppercase">ШАХТИНСКАЯ ПЛИТКА</span>
                                    <p>Занимает ведущие позиции на российском рынке керамических отделочных материалов с 2005 года</p>
                                </div>
                            </div>
                            <div class="item <?php if($activeSlider['slider']['select']==3) echo 'active'?>">
                                <img src="/img/slider/keramamarazzi.jpg" alt="KERAMA MARAZZI">
                                <div class="carousel-caption">
                                    <span style="font-size: 24pt;text-transform: uppercase">KERAMA MARAZZI</span>
                                    <p>Лучшие коллекции этого года</p>
                                </div>
                            </div>
                            <div class="item <?php if($activeSlider['slider']['select']==4) echo 'active'?>">
                                <img src="/img/slider/graciaceramica.jpg" alt="Gracia Ceramica">
                                <div class="carousel-caption">
                                    <span style="font-size: 24pt;text-transform: uppercase">Gracia Ceramica</span>
                                    <p>Керамическая плитка европейского качества для ванной, кухни, коридора, напольная плитка</p>
                                </div>
                            </div>
                            <div class="item <?php if($activeSlider['slider']['select']==5) echo 'active'?>">
                                <img src="/img/slider/intercerama.jpg" alt="Интеркерама">
                                <div class="carousel-caption">
                                    <span style="font-size: 24pt;text-transform: uppercase">Интеркерама</span>
                                    <p>Ведущий производитель качественной керамической плитки, ориентированный на современные запросы рынка керамической плитки</p>
                                </div>
                            </div>
                            <div class="item <?php if($activeSlider['slider']['select']==6) echo 'active'?>">
                                <img src="/img/slider/cersanit.jpg" alt="Cersanit">
                                <div class="carousel-caption">
                                    <span style="font-size: 24pt;text-transform: uppercase">Cersanit</span>
                                    <p>Польская фирма по производству керамических изделий и сантехнического оборудования</p>
                                </div>
                            </div>
                            <div class="item <?php if($activeSlider['slider']['select']==7) echo 'active'?>">
                                <img src="/img/slider/lasselsberger.jpg" alt="Lasselsberger Ceramics">
                                <div class="carousel-caption">
                                    <span style="font-size: 24pt;text-transform: uppercase">Lasselsberger Ceramics</span>
                                    <p>Крупнейший мировой холдинг по производству керамической плитки и керамогранита</p>
                                </div>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>

            <!--<img src="/img/background/column.png" alt="" class="column">-->
            <!--<div class="row">-->
            <!--<dov class="col-md-12"><a href="/"><img src="/img/logo.png" alt="КАПРИЗ - КЕРАМИК" class="logo"></a></dov>-->
            <!--</div>-->
        </div>
    </div>
    <!--<div class="plane"></div>-->
    <!--<div class="plants"></div>-->
</header>
<main class="main container">
    <?php
        $this->display($this->data['display']);
    ?>
</main>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <address>
                    <a href="#"><img src="/img/logo-footer.png" alt="КАПРИЗ - КЕРАМИК" class="logo"></a><br>
                    <abbr title="Городской телефон">Тел:</abbr> <a href="tel: 88452649345" style="color: #fff">8 (8452) 64-93-45</a><br>
                    <abbr title="Мобильный телефон">Моб:</abbr> <a href="tel: 89271193030" style="color: #fff">8 927 119-30-30</a><br>
                    <abbr title="Электронная почта">E-mail:</abbr> <a href="mailto: kapriz-keramik@ya.ru" style="color: #fff">kapriz-keramik@ya.ru</a><br>
                    <abbr title="Наш адрес">Адрес:</abbr> г. Саратов, ул. Университетская 109 (угол Соколовой)<br>
                    <abbr title="Режим работы">Режим работы:</abbr> С 09.00 до 19.00; Сб - Вс с 10.00 до 17.00
                </address>
            </div>
            <div class="col-md-3 col-xs-6">
                <ul class="page">
                    <li><a href="/">О компании</a></li>
                    <li><a href="#">Каталог</a></li>
                    <li><a href="/index/page/shares">Акции</a></li>
                    <li><a href="/index/page/contacts">Контакты</a></li>
                    <li><a href="/index/page/payment">Доставка</a></li>
                    <li><a href="/reviews">Отзывы</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-6">
                <ul>
                    <li><a href="/category/view/plitka_dlya_vannoy">Плитка для ванной</a></li>
                    <li><a href="/category/view/plitka_dlya_kukhni_v_saratove">Плитка для кухни</a></li>
                    <li><a href="/category/view/plitka_dlya_pola_v_saratove">Плитка для пола</a></li>
                    <li><a href="/category/view/plitka_mozaika_v_saratove">Плитка мозаика</a></li>
                    <li><a href="/category/view/keramogranit_v_saratove">Плитка керамогранит</a></li>
                    <li><a href="/category/view/oblitsovochnyy_kamen_v_saratove">Облицовочный камень</a></li>
                    <li><a href="/category/view/plitka_klinker_v_saratove">Клинкер</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <p class="text-center">
            Copyright © 1998-2017, Компания «Каприз» керамическая плитка в Саратове . Все права защищены.
        </p>
    </div>
</footer>
<div class="container">

    <?php if ($_SERVER['REQUEST_URI']!='/cart'): ?>
        <div id="cart" class="panel panel-default" data-display="<?php
        if(!empty($_SESSION['cart']['id'])&&count($_SESSION['cart']['id'])){
            echo $_SESSION['cart']['active'];
        } else {
            echo 'none';
        }
        ?>"
             style="display: <?php
                if(!empty($_SESSION['cart']['id'])&&count($_SESSION['cart']['id'])){
                    echo $_SESSION['cart']['active'];
                } else {
                    echo 'none';
                }
             ?>">
            <div class="panel-body" style="width: 320px; padding: 10px">
                <div class="row">
                    <div class="col-xs-4">
                        <a href="/cart"><img src="/img/ico/cart.png" alt="Корзина" width="30"></a>
                        <span id="numCart">
                            <?php if (isset($_SESSION['cart']['num'])) {
                                $totalNum = 0;
                                foreach($_SESSION['cart']['num'] as $key => $num){
                                    $totalNum += $num;
                                }
                                echo $totalNum;
                            } ?>
                        </span>
                        шт.
                    </div>
                    <div class="col-xs-4" style="position: relative;top: 2px;">
                        <p><span id="costCart" style="font-weight: 600">
                                <?php if (isset($_SESSION['cart']['cost'])) {
                                    $totalCost = 0;
                                    foreach($_SESSION['cart']['cost'] as $key => $cost){
                                        $totalCost += $cost*$_SESSION['cart']['num'][$key];
                                    }
                                    echo $totalCost;
                                } ?>
                            </span>
                            руб.</p>
                    </div>
                    <div class="col-xs-4">
                        <a href="/cart" class="btn btn-danger btn-sm">Оформить</a>
                    </div>
                </div>
            </div>
        </div>
    <? endif ?>
</div>

<div class="modal fade bs-example-modal-lg" id="modalBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Изображение</h4>
            </div>
            <div class="modal-body">
                <img src="/img/Kapriz_salon_magazin800_1.png" alt="" id="modalImg" style="width: 100%">
            </div>
        </div>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
<!--<script src="/js/jQuery.scrollSpeed.js"></script>-->
<script src="/js/cart.js"></script>
<script src="/js/callme.js"></script>
<script src="/js/reviews.js"></script>
<script src="/js/script.js"></script>
<!-- Подключение JS файла Fancybox -->
<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("a.fancyimage").fancybox();
        if(document.location.href != 'http://kapriz-keramik.ru/'&&document.location.href != 'http://www.kapriz-keramik.ru/'&&document.location.href != 'http://kapriz-keramik.ru/index.php'&&document.location.href != 'http://www.kapriz-keramik.ru/index.php'){
            $('body,html').animate({"scrollTop":660},"slow");
        }
    });
</script>
<script>
    <?php if($activeSlider['slider']['status']=='stop'){?>
    $(window).load(function() {
        $('.carousel').carousel('pause');
    });
    <?php }?>
</script>
<!-- Yandex.Metrika counter -->
<noscript>
    <div><img src="https://mc.yandex.ru/watch/38690790" id="wb_uid54" alt=""/></div>
</noscript><!-- /Yandex.Metrika counter --><!-- Rating@Mail.ru counter -->
<noscript>
    <div id="wb_uid55"><img src="//top-fwz1.mail.ru/counter?id=2802181;js=na" height="1" width="1"
                            alt="Рейтинг@Mail.ru"/></div>
</noscript><!-- //Rating@Mail.ru counter -->
</body>
</html>
