<?php //$order = $this->data['orders'];
//var_dump($this->data['collection']);
//var_dump($this->data['product']);
?>
<h1 class="page-header">Заказ №<?php echo $this->data['orderProduct'][0]->order_id?></h1>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#id</th>
                <th>Коллекция</th>
                <th>Наименование</th>
                <th>Количество</th>
                <th>Стоимость</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['orderProduct'] as $key => $item){ ?>
                <tr>
                    <td>
                        <?php echo $item->id ?>
                    </td>
                    <td>
                        <?php echo $this->data['collection'][$key]->name ?>
                    </td>
                    <td>
                        <?php echo $this->data['product'][$key]->name?>
                    </td>
                    <td>
                        <?php echo $item->num?>
                    </td>
                    <td>
                        <?php echo $item->cost?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>