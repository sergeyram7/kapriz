var CallMe = {
    validator: function(){
        var phone = document.getElementById('callme-phone').value;
        var reg=/^((8|\+7)[\- ]?)?(\(?\d{3,4}\)?[\- ]?)?[\d\- ]{7,10}$/;
        if (reg.test(phone)) {
            send(phone);
        } else {
            alert('Ошибка! Неверный формат, попробуйте ещё раз');
        }
    },
    url: '/function/send.php',
};

function send(phone){
    jQuery.ajax({
        url:     CallMe.url, //Адрес подгружаемой страницы
        type:     "GET", //Тип запроса
        dataType: "html", //Тип данных
        data: "action="+'callme'+"&phone="+phone,
        success: function(response) { //Если все нормально
            alert('Спасибо! Заявка была отправлена, вскоре мы Вам перезвоним.');
        },
        error: function(response) { //Если ошибка
            alert('Ошибка! Свяжитесь пожалуйста с администратором');
        }
    });
}