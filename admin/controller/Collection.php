<?php

namespace Admin\Controller;
use Admin\Model\Category;
use Admin\Model\Collection as CollectionModel;
use Admin\Model\Product;
use Admin\Model\Producer;
use Admin\View\View;
header('Content-Type: text/html');
//var_dump('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
class Collection{

    public function action_all(){
        $collection = CollectionModel::getAll();
        $countProduct = [];
        $categoryName = [];
        $producerName = [];
        for($i=0;isset($collection[$i]);$i++){
            $countProduct[$i] = Product::getNumAll(['collection_id'=>$collection[$i]->id]);
            $categoryName[$i] = Category::getSome(1,['id'=>$collection[$i]->category_id]);
            $producerName[$i] = Producer::getSome(1,['id'=>$collection[$i]->producer_id]);
        }
        $view = new View();
        $view->assign('collection', $collection);
        $view->assign('countProduct',$countProduct);
        $view->assign('categoryName',$categoryName);
        $view->assign('producerName',$producerName);
        $view->assign('pageTitle', 'Коллекции' . ' :: ');
        $view->assign('display', 'collection/default.php');
        $view->template();
    }

    public function action_edit(){
        if($_POST){
            //обработка изображения
            $imgName = $this->uploadImg('img');
            $parameters = [ 'name'=>$_POST['name'],
                            'alias'=>$_POST['alias'],
                            'short_description'=>str_replace("'","&rsquo;",$_POST['short_description']),
                            'long_description'=>str_replace("'","&rsquo;",$_POST['long_description']),
                            'characteristics'=>str_replace("'","&rsquo;",$_POST['characteristics']),
                            'category_id'=>$_POST['category_id'],
                            'producer_id'=>$_POST['producer_id'],
                            'isRecommended'=>$_POST['isRecommended'],
                            'metaKeywords'=>$_POST['metaKeywords'],
                            'metaDescription'=>$_POST['metaDescription']];
            if ($_FILES['img']['name']) {
                $parameters['img'] = $imgName;
            }
            //var_dump($parameters);
            CollectionModel::update($parameters,['id'=>$_GET['id']]);
            //добавление продуктов
            for ($a = 0; !empty($_POST['product_name'][$a]); $a++) {
                //обработка изображения товаров
                if (!empty($_FILES["product_img"]['name'][$a])) {
                    $imgFile = $this->uploadArrayImg("product_img", $a);
                } else {
                    $imgFile = $_FILES["product_img"]['name'][$a];
                }
                $parameters = [ 'name'=>$_POST['product_name'][$a],
                    'collection_id'=>$_GET['id'],
                    'width'=>$_POST['product_width'][$a],
                    'height'=>$_POST['product_height'][$a],
                    'cost'=>$_POST['product_cost'][$a],
                    'costOf'=>$_POST['product_costOf'][$a],
                    'unit'=>$_POST['product_unit'][$a]];
                if (!empty($imgFile)) {
                    $parameters["img"] = $imgFile;
                }
                //var_dump($parameters);
                if (isset($_POST['product_id'][$a])) {
                    Product::update($parameters, ['id' => $_POST['product_id'][$a]]);
//                    echo 'Делаем update';
                } else {
                    Product::add($parameters);
//                    echo 'Делаем add';
                }
            }
            $collection = CollectionModel::getSome(1,['id'=>$_GET['id']])[0];
            $selectCategory = Category::getSome(1,['id'=>$collection->category_id]);
            $category = Category::getAll();
            $selectProducer = Producer::getSome(1,['id'=>$collection->producer_id]);
            $producer = Producer::getAll();
            $product = Product::getAll(['collection_id'=>$collection->id]);
            $view = new View();
            $view->assign('collection', $collection);
            $view->assign('selectCategory', $selectCategory);
            $view->assign('selectProducer', $selectProducer);
            $view->assign('category', $category);
            $view->assign('producer', $producer);
            $view->assign('product', $product);
            $view->assign('save', true);
            $view->assign('pageTitle', 'Редактирование коллекции:' .$collection->name. ' :: ');
            $view->assign('display', 'collection/edit.php');
            $view->template();
        } else{
            $collection = CollectionModel::getSome(1,['id'=>$_GET['id']])[0];
            $selectCategory = Category::getSome(1,['id'=>$collection->category_id]);
            $category = Category::getAll();
            $selectProducer = Producer::getSome(1,['id'=>$collection->producer_id]);
            $producer = Producer::getAll();
            $product = Product::getAll(['collection_id'=>$collection->id]);
            $view = new View();
            $view->assign('collection', $collection);
            $view->assign('selectCategory', $selectCategory);
            $view->assign('selectProducer', $selectProducer);
            $view->assign('category', $category);
            $view->assign('producer', $producer);
            $view->assign('product', $product);
            $view->assign('pageTitle', 'Редактирование коллекции:' .$collection->name. ' :: ');
            $view->assign('display', 'collection/edit.php');
            $view->template();
        }
    }

    public function action_add(){
        if($_POST){
            //обработка изображения
            $imgFile = $this->uploadImg('img');
            $parameters = [ 'name'=>$_POST['name'],
                'alias'=>$_POST['alias'],
                'short_description'=>str_replace("'","&rsquo;",$_POST['short_description']),
                'long_description'=>str_replace("'","&rsquo;",$_POST['long_description']),
                'characteristics'=>str_replace("'","&rsquo;",$_POST['characteristics']),
                'category_id'=>$_POST['category_id'],
                'producer_id'=>$_POST['producer_id'],
                'isRecommended'=>$_POST['isRecommended'],
                'metaKeywords'=>$_POST['metaKeywords'],
                'metaDescription'=>$_POST['metaDescription']];
            if ($_FILES['img']['name']) {
                $parameters['img'] = $imgFile;
            }
            //var_dump($parameters);
            $lastIdCollection = CollectionModel::add($parameters);
            //добавление продуктов
            for ($a = 0; !empty($_POST['product_name'][$a]); $a++) {
                //обработка изображения товаров
                if (!empty($_FILES["product_img"]['name'][$a])) {
                    $imgFile = $this->uploadArrayImg("product_img", $a);
                } else {
                    $imgFile = $_FILES["product_img"]['name'][$a];
                }
                $parameters = [ 'name'=>$_POST['product_name'][$a],
                    'collection_id'=>$lastIdCollection,
                    'width'=>$_POST['product_width'][$a],
                    'height'=>$_POST['product_height'][$a],
                    'cost'=>$_POST['product_cost'][$a],
                    'costOf'=>$_POST['product_costOf'][$a],
                    'unit'=>$_POST['product_unit'][$a]];
                if (!empty($imgFile)) {
                    $parameters["img"] = $imgFile;
                }
                //var_dump($parameters);
                if (isset($_POST['product_id'][$a])) {
                    Product::update($parameters, ['id' => $_POST['product_id'][$a]]);
//                    echo 'Делаем update';
                } else {
                    Product::add($parameters);
//                    echo 'Делаем add';
                }
            }
//            $collection = CollectionModel::getSome(1,['id'=>$_GET['id']])[0];
//            $selectCategory = Category::getSome(1,['id'=>$collection->category_id]);
//            $category = Category::getAll();
//            $selectProducer = Producer::getSome(1,['id'=>$collection->producer_id]);
//            $producer = Producer::getAll();
//            $product = Product::getAll(['collection_id'=>$collection->id]);

            $view = new View();
//            $view->assign('collection', $collection);
//            $view->assign('selectCategory', $selectCategory);
//            $view->assign('selectProducer', $selectProducer);
//            $view->assign('category', $category);
//            $view->assign('producer', $producer);
//            $view->assign('product', $product);
            $view->assign('save', true);
            $view->assign('pageTitle', 'Коллекция добавлена :: ');
            $view->assign('display', 'collection/addSuccess.php');
            $view->template();
        } else{
            //$collection = CollectionModel::getSome(1,['id'=>$_GET['id']])[0];
            //$selectCategory = Category::getSome(1,['id'=>$collection->category_id]);
            $category = Category::getAll();
            //$selectProducer = Producer::getSome(1,['id'=>$collection->producer_id]);
            $producer = Producer::getAll();
            //$product = Product::getAll(['collection_id'=>$collection->id]);
            $view = new View();
//            $view->assign('collection', $collection);
//            $view->assign('selectCategory', $selectCategory);
//            $view->assign('selectProducer', $selectProducer);
            $view->assign('category', $category);
            $view->assign('producer', $producer);
//            $view->assign('product', $product);
            $view->assign('pageTitle', 'Добавление коллекции :: ');
            $view->assign('display', 'collection/add.php');
            $view->template();
        }
    }

    private function uploadImg($name){
        //var_dump(empty($_FILES[$name]['name']));
        $uploaddir = __DIR__ . '/../../img/collection/';
        if (file_exists($uploaddir.$_FILES[$name]['name'])) {
            $fileName = explode('.',$_FILES[$name]['name']);
            $fileName[0] = rand(1, 500000);
            $fileName = implode('.',$fileName);
            $fileName = $this->translit($fileName);
            //var_dump($fileName);
        } else {
            $fileName = $this->translit($_FILES[$name]['name']);
        }
        $uploadfile = $uploaddir . basename($fileName);
        move_uploaded_file($_FILES[$name]['tmp_name'], $uploadfile);
        if(!empty($_FILES[$name]['name'])){
            $image = new \Simpleimage();
            $image->load($uploadfile);
            $image->resizeToWidth(260); // В аргумент ширину картинки, которая нужна(Она пропорц. уменьш.)
            $image->save($uploaddir . 'min/' . basename($fileName)); // Сохраняем
        }

//        $this->resizeImg($name,$uploadfile);
        return $fileName;
    }

//    private function resizeImg($name,$filename){
//        var_dump($filename);
//        $width = 260;
//        $height = 180;
//
//        // тип содержимого
//        //header('Content-Type: image/jpeg');
//
//        // получение новых размеров
//        list($width_orig, $height_orig) = getimagesize($filename);
//
//        $ratio_orig = $width_orig/$height_orig;
//
//        if ($width/$height > $ratio_orig) {
//            $width = $height*$ratio_orig;
//        } else {
//            $height = $width/$ratio_orig;
//        }
//
//        var_dump($width);
//        var_dump($height);
//
//        // ресэмплирование
//        $image_p = imagecreatetruecolor($width, $height);
//        $image = imagecreatefromjpeg($filename);
//        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
//
//        var_dump($image);
//
//
//        image2wbmp($image,$filename . '.min');
//
//        //move_uploaded_file($image_p, $filename . '.min');
//    }

    private function uploadArrayImg($name,$num){
        $uploaddir = __DIR__ . '/../../img/product/';
        if (file_exists($uploaddir.$_FILES[$name]['name'][$num])) {
            $fileName = explode('.',$_FILES[$name]['name'][$num]);
            $fileName[0] = rand(1, 500000);
            $fileName = implode('.',$fileName);
            $fileName = $this->translit($fileName);
            //var_dump($fileName);
        } else {
            $fileName = $this->translit($_FILES[$name]['name'][$num]);
        }
        $uploadfile = $uploaddir . basename($fileName);
        move_uploaded_file($_FILES[$name]['tmp_name'][$num], $uploadfile);
        return $fileName;
    }
    function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = str_replace(' ', '_', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_. ]/i", "", $s); // очищаем строку от недопустимых символов
        return $s; // возвращаем результат
    }
}