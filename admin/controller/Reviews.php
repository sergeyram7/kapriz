<?php

namespace Admin\Controller;
use Admin\Model\Reviews as ReviewsModel;
use Admin\View\View;

class Reviews
{
    public function action_all(){
        $items = ReviewsModel::getAll();
        $view = new View();
        $view->assign('reviews', $items);
        $view->assign('pageTitle', 'Отзывы :: ');
        $view->assign('display', 'reviews/default.php');
        $view->template();
        //$view->display('reviews/all.php');
    }
//    public function action_some($num,$where=[],$sorting=[]){
//        $items = ReviewsModel::getSome((int)$num,$where,$sorting);
//        $view = new View();
//        $view->assign('reviews', $items);
//        $view->display('reviews/simple.php');
//    }
//
//    public function action_view(){
//        $items = ReviewsModel::getOne((int)$_GET['id']);
//        $view = new View();
//        $view->assign('reviews', $items[0]);
//        $view->assign('pageTitle', $items[0]->title . ' :: Отзывы :: ');
//        $view->assign('display', 'reviews/one.php');
//        $view->template();
//    }
//
//    public function action_count(){
//        return ReviewsModel::getNumAll();
//    }
//
//    public function action_text(){
//        if(!empty($_GET['city'])) {
//            $cityNowId = \App\Model\City::getBy('alias',$_GET['city'])->id;
//            $where['city'] = $cityNowId;
//        } else {
//            $where['city']=0;
//        }
//        $items = TextModel::getBy('city',$where['city']);
//        if(!$items) $items = TextModel::getBy('city','0');
//        $view = new View();
//        $view->assign('text', $items);
//        $view->display('text/main.php');
//    }

}