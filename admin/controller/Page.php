<?php

namespace Admin\Controller;
use Admin\Model\Category as CategoryModel;
use Admin\View\View;

class Page{
    public function action_all(){
        $page = \Admin\Model\Page::getAll();
        $view = new View();
        $view->assign('page', $page);
        $view->assign('pageTitle', 'Страницы' . ' :: ');
        $view->assign('display', 'page/default.php');
        $view->template();
    }
    public function action_edit(){
        if($_POST){
            \Admin\Model\Page::update(['name'=>$_POST['name'],'alias'=>$_POST['alias'],'text'=>str_replace("'","&rsquo;",$_POST['text']),'metaKeywords'=>$_POST['metaKeywords'],'metaDescription'=>$_POST['metaDescription']],['id'=>$_GET['id']]);
            $page = \Admin\Model\Page::getSome(1,['id'=>$_GET['id']]);
            $view = new View();
            $view->assign('page', $page);
            $view->assign('save', true);
            $view->assign('pageTitle', 'Редактирование страницы:' .$page[0]->name. ' :: ');
            $view->assign('display', 'page/edit.php');
            $view->template();
        } else{
            $page = \Admin\Model\Page::getSome(1,['id'=>$_GET['id']]);
            $view = new View();
            $view->assign('page', $page);
            $view->assign('pageTitle', 'Редактирование страницы:' .$page[0]->name. ' :: ');
            $view->assign('display', 'page/edit.php');
            $view->template();
        }

    }
}