<div class="row">
    <div class="col-md-12">
        <h2>Все категории</h2>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#id</th>
                <th>Название</th>
                <th>Заголовок</th>
                <th>Алиас</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['category'] as $key => $category){ ?>
                <tr>
                    <td>
                        <?php echo $category->id ?>
                    </td>
                    <td>
                        <?php echo $category->name ?>
                    </td>
                    <td>
                        <?php echo $category->title ?>
                    </td>
                    <td>
                        <?php echo $category->alias ?>
                    </td>
                    <td>
                        <a href="/admin/category/edit/<?php echo $category->id ?>" class="btn btn-default">Редактировать</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>