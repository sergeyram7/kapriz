<div class="row">
    <div class="col-md-12">
        <h2>Все коллекции</h2>
        <a href="/admin/collection/add" class="btn btn-success">+ Добавить</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#id</th>
                <th>Название</th>
                <th>Описание</th>
                <th>Категория</th>
                <th>Производитель</th>
                <th>Позиций</th>
                <th>Рекоменд.</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['collection'] as $key => $collection){ ?>
                <tr id="item<?php echo $collection->id ?>">
                    <td>
                        <?php echo $collection->id ?>
                    </td>
                    <td>
                        <?php echo $collection->name ?>
                    </td>
                    <td>
                        <?php echo substr($collection->short_description, 0, 20)?>
                    </td>
                    <td>
                        <?php echo $this->data['categoryName'][$key][0]->name?>
                    </td>
                    <td>
                        <?php echo $this->data['producerName'][$key][0]->name?>
                    </td>
                    <td>
                        <?php echo $this->data['countProduct'][$key]?>
                    </td>
                    <td>
                        <?php if($collection->isRecommended){
                            echo '+';
                        } else{
                            echo '-';
                        }?>
                    </td>
                    <td>
                        <a href="/admin/collection/edit/<?php echo $collection->id ?>" class="btn btn-default">Редактировать</a>
                        <button type="button" onclick="Collection.removeItem(<?php echo $collection->id ?>)" class="btn btn-danger">Удалить</button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>