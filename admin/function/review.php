<?php
session_start();

require_once '../model/DBConfig.php';

if (isset($_SESSION["is_auth"])) {
  if ($_GET['action'] == 'remove') {
    $mysqli = new mysqli(DB_HOST, DB_LOGIN, DB_PASS, DB_NAME);
    $sql = 'DELETE FROM reviews WHERE id = ' . $_GET['id'];
    $res = $mysqli->query($sql);
  }
  if ($_GET['action'] == 'publish') {
    $mysqli = new mysqli(DB_HOST, DB_LOGIN, DB_PASS, DB_NAME);
    $sql = 'UPDATE reviews SET isPublic=1 WHERE id = ' . $_GET['id'];
    $res = $mysqli->query($sql);
  }
  if ($_GET['action'] == 'unPublish') {
    $mysqli = new mysqli(DB_HOST, DB_LOGIN, DB_PASS, DB_NAME);
    $sql = 'UPDATE reviews SET isPublic=0 WHERE id = ' . $_GET['id'];
    $res = $mysqli->query($sql);
  }
}