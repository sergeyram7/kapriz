<h1 class="page-header">Настройка слайдера</h1>
<div class="row">
    <div class="col-md-12">
        <form method="post">
            <div class="radio">
                <label>
                    <input type="radio" name="status" id="statusStart" value="start" <?php if($this->data['slider']['status']=='start') echo 'checked'?>>
                    Включена автоматическая прокрутка слайдера
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="status" id="statusStop" value="stop" <?php if($this->data['slider']['status']=='stop') echo 'checked'?>>
                    Отключена автоматическая прокрутка слайдера
                </label>
            </div>
            <div class="form-group">
                <label for="selectSlide">Первичный слайд</label>
                <select id="selectSlide" name="selectSlide" class="form-control">
                    <option value="0" <?php if($this->data['slider']['select']===0) echo 'selected'?>>Уралкерамика</option>
                    <option value="1" <?php if($this->data['slider']['select']==1) echo 'selected'?>>Березакерамика</option>
                    <option value="2" <?php if($this->data['slider']['select']==2) echo 'selected'?>>ШАХТИНСКАЯ ПЛИТКА</option>
                    <option value="3" <?php if($this->data['slider']['select']==3) echo 'selected'?>>KERAMA MARAZZI</option>
                    <option value="4" <?php if($this->data['slider']['select']==4) echo 'selected'?>>Gracia Ceramica</option>
                    <option value="5" <?php if($this->data['slider']['select']==5) echo 'selected'?>>Интеркерама</option>
                    <option value="6" <?php if($this->data['slider']['select']==6) echo 'selected'?>>Cersanit</option>
                    <option value="7" <?php if($this->data['slider']['select']==7) echo 'selected'?>>Lasselsberger Ceramics</option>
                </select>
            </div>
            <button class="btn btn-success" type="submit">Сохранить</button>
        </form>
    </div>
</div>