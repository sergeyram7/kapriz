<?php
function autoloader($class){
    if(file_exists(__DIR__ . '/classes/' . $class . '.php')){
        require_once __DIR__ . '/classes/' . $class . '.php';
    } else if(file_exists(__DIR__ . '/function/' . $class . '.php')){
        require_once __DIR__ . '/function/' . $class . '.php';
    } else if(file_exists(__DIR__ . '/model/' . $class . '.php')){
        require_once __DIR__ . '/model/' . $class . '.php';
    } else if(file_exists(__DIR__ . '/view/' . $class . '.php')){
        require_once __DIR__ . '/view/' . $class . '.php';
    } else if(file_exists(__DIR__ . '/controller/' . $class . '.php')){
        require_once __DIR__ . '/controller/' . $class . '.php';
    } else {
        $src = explode('\\',$class);
        $src[0] = __DIR__;
        $src[1] = lcfirst($src[1]);
        $src[2] .= '.php';
        $path = implode(DIRECTORY_SEPARATOR,$src);
//        var_dump($class);
//        var_dump($path);
        //(require_once $path) or die('404! b');
        (@include_once $path) or die(error404());
    }
}

function error404(){
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    include_once __DIR__ .'/404.html';
    //header("Location: http://kapriz.local/404.html");
    return false;
}

spl_autoload_register('autoloader');