<?php

namespace Admin\Controller;
use Admin\Model\Category as CategoryModel;
use Admin\View\View;

class Category{
    public function action_all(){
        $category = CategoryModel::getAll();
        $view = new View();
        $view->assign('category', $category);
        $view->assign('pageTitle', 'Категории' . ' :: ');
        $view->assign('display', 'category/default.php');
        $view->template();
    }
    public function action_edit(){
        if($_POST){
            CategoryModel::update(['name'=>$_POST['name'],'title'=>$_POST['title'],'alias'=>$_POST['alias'],'description'=>str_replace("'","&rsquo;",$_POST['description']),'metaKeywords'=>$_POST['metaKeywords'],'metaDescription'=>$_POST['metaDescription']],['id'=>$_GET['id']]);
            $category = CategoryModel::getSome(1,['id'=>$_GET['id']]);
            $view = new View();
            $view->assign('category', $category);
            $view->assign('save', true);
            $view->assign('pageTitle', 'Редактирование категории:' .$category[0]->name. ' :: ');
            $view->assign('display', 'category/edit.php');
            $view->template();
        } else{
            $category = CategoryModel::getSome(1,['id'=>$_GET['id']]);
            $view = new View();
            $view->assign('category', $category);
            $view->assign('pageTitle', 'Редактирование категории:' .$category[0]->name. ' :: ');
            $view->assign('display', 'category/edit.php');
            $view->template();
        }

    }
}