<?php

namespace App\Controller;
use App\Model\Category;
use App\Model\Page;
use App\Model\Collection;
use App\Model\Product;
use App\Model\Producer;
use App\View\View;

//session_start();

class Cart
{
    public function action_all($where=[]){
        if(empty($where)) $where=[];
//        $page = Page::getSome(1,['alias'=>'index'],[]);
//        $collection = Collection::getSome(4,['isRecommended'=>'1'],[]);
//        for($i=0;isset($collection[$i]);$i++){
//            $countProduct[$i] = Product::getNumAll(['collection_id'=>$collection[$i]->id]);
//        }
////        $countProduct = Product::getNumAll(['collection_id'=>$collection[0]->id]);
//        for($i=0;isset($collection[$i]);$i++){
//            $producer[$i] = Producer::getSome(1,['id'=>$collection[$i]->producer_id],[])[0];
//        }
//        //$producer = Producer::getSome(1,['id'=>$collection[0]->producer_id],[]);
//        for($i=0;isset($collection[$i]);$i++){
//            $category[$i] = Category::getSome(1,['id'=>$collection[$i]->category_id],[])[0];
//        }
        if (isset($_SESSION['cart']['id'])) {
            $product = [];
            $collection = [];
            $producer = [];
            $category = [];
            foreach ($_SESSION['cart']['id'] as $itemId) {
                $getItem = Product::getSome(1, ['id' => $itemId], [])[0];
                $getCollection = Collection::getSome(1, ['id' => $getItem->collection_id], [])[0];
                $getProducer = Producer::getSome(1, ['id' => $getCollection->producer_id], [])[0];
                $getCategory = Category::getSome(1, ['id' => $getCollection->category_id], [])[0];
                array_push($product, $getItem);
                array_push($collection, $getCollection);
                array_push($producer, $getProducer);
                array_push($category, $getCategory);
            }
        }
        $view = new View();
//        $view->assign('page',$page);
        $view->assign('collection',$collection);
//        $view->assign('countProduct',$countProduct);
        $view->assign('producer',$producer);
        $view->assign('product',$product);
        $view->assign('category',$category);
        $view->assign('pageTitle', 'Ваша корзина :: ');
        $view->assign('display', 'cart/default.php');
        $view->template();
    }
//    public function action_page(){
//        if ($_GET['alias']) {
//            $page = Page::getSome(1, ['alias' => $_GET['alias']], []);
//            $view = new View();
//            $view->assign('page', $page);
//            $view->assign('pageTitle', $page[0]->name . ' :: ');
//            $view->assign('display', 'page/page.php');
//            $view->template();
//        } else {
//            echo '404';
//        }
//    }
}