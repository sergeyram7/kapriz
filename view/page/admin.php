<h1 class="page-header">Главная</h1>
<div class="row">
    <div class="col-md-12">
        <h2>Последние коллекции</h2>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#id</th>
                <th>Название</th>
                <th>Описание</th>
                <th>Категория</th>
                <th>Производитель</th>
                <th>Позиций</th>
                <th>Рекоменд.</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['collection'] as $key => $collection){ ?>
            <tr>
                <td>
                    <?php echo $collection->id ?>
                </td>
                <td>
                    <?php echo $collection->name ?>
                </td>
                <td>
                    <?php echo substr($collection->short_description, 0, 20)?>
                </td>
                <td>
                    <?php echo $this->data['categoryName'][$key][0]->name?>
                </td>
                <td>
                    <?php echo $this->data['producerName'][$key][0]->name?>
                </td>
                <td>
                    <?php echo $this->data['countProduct'][$key]?>
                </td>
                <td>
                    <?php if($collection->isRecommended){
                        echo '+';
                    } else{
                        echo '-';
                    }?>
                </td>
                <td>
                    <button type="button" class="btn btn-default">Edit</button>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <h2>Последние заказы</h2>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#id</th>
                <th>ФИО</th>
                <th>Телефон</th>
                <th>Email</th>
                <th>Адрес</th>
                <th>Дата и время</th>
                <th>Способ оплаты</th>
                <th>Статус</th>
                <th>Общая стоимость</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['orders'] as $key => $order){ ?>
                <tr>
                    <td>
                        <?php echo $order->id ?>
                    </td>
                    <td>
                        <?php echo $order->userName ?>
                    </td>
                    <td>
                        <?php echo $order->phone?>
                    </td>
                    <td>
                        <?php echo $order->email?>
                    </td>
                    <td>
                        <?php echo $order->address?>
                    </td>
                    <td>
                        <?php echo $order->dateTime?>
                    </td>
                    <td>
                        <?php if($order->payMethod == 'cash') {
                            echo 'Наличные';
                        } else {
                            echo 'Безналичные';
                        }?>
                    </td>
                    <td>
                        <?php if($order->status){
                            echo '<span class="label label-success">Обработан</span>';
                        } else{
                            echo '<span class="label label-danger">Не обработан</span>';
                        }?>
                    </td>
                    <td>
                        <?php echo $order->orderProductCost?>
                    </td>
                    <td>
                        <button type="button" class="btn btn-default">Edit</button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>