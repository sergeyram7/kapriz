<?php

namespace Admin\Controller;
use Admin\Model\Callme;
use Admin\Model\Category;
use Admin\Model\Orders;
use Admin\Model\OrderProduct;
use Admin\Model\Collection;
use Admin\Model\Product;
use Admin\Model\Producer;
use Admin\View\View;

class Index
{
    public function action_all($where=[]){
        if(empty($where)) $where=[];
        $collection = Collection::getSome(10,[],['id'=>'DESC']);
        $orders = Orders::getSome(10,[],['id'=>'DESC']);
        $callme = Callme::getSome(10,[],['id'=>'DESC']);
        //var_dump($orders);
        for($i=0;isset($collection[$i]);$i++){
            $countProduct[$i] = Product::getNumAll(['collection_id'=>$collection[$i]->id]);
            $categoryName[$i] = Category::getSome(1,['id'=>$collection[$i]->category_id]);
            $producerName[$i] = Producer::getSome(1,['id'=>$collection[$i]->producer_id]);
        }
        for($i=0;isset($orders[$i]);$i++){
            $countOrder[$i] = OrderProduct::getNumAll(['order_id'=>$orders[$i]->id]);
        }
        $view = new View();
        $view->assign('collection',$collection);
        $view->assign('orders',$orders);
        $view->assign('callme',$callme);
        $view->assign('countOrder',$countOrder);
        $view->assign('countProduct',$countProduct);
        $view->assign('categoryName',$categoryName);
        $view->assign('producerName',$producerName);
        $view->assign('pageTitle', 'Главная страница :: ');
        $view->assign('display', 'page/admin.php');
        $view->template();
    }
}