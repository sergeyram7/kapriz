<div class="row">
    <div class="col-xs-12">
        <h1 class="underline-header"><?php echo $this->data['category'][0]->name?> от "<?php echo $this->data['producer'][0]->name?>"</h1>
        <div class="row">
            <?php
            foreach($this->data['collection'] as $key => $collection){?>
                <div class="col-md-3">
                    <div class="thumbnail">
                        <div class="wrap-img"><a href="/category/view/<?php echo $this->data['category'][0]->alias . '/' . $this->data['producer'][0]->alias . '/' . $collection->id . '/' . $collection->alias?>"><img src="/img/collection/min/<?php echo $collection->img?>" alt="<?php echo $collection->name?>"></a></div>
                        <div class="caption">
                            <h3><a href="/category/view/<?php echo $this->data['category'][0]->alias . '/' . $this->data['producer'][0]->alias . '/' . $collection->id . '/' . $collection->alias?>"><?php echo $collection->name?></a></h3>
                            <p class="short_description"><?php echo $collection->short_description?></p>
                            <ul>
                                <li><strong>Позиций в коллекции: </strong><?php echo($this->data['countProduct'][$key])?></li>
                            </ul>
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="/producer/view/<?php echo($this->data['producer'][0]->alias)?>" class="btn btn-link btn-producer" role="button" title="Посмотреть все коллекции от '<?php echo($this->data['producer'][0]->name)?>'"><?php echo $this->data['producer'][0]->name;?></a>
                                </div>
                                <div class="col-xs-12">
                                    <a href="/category/view/<?php echo $this->data['category'][0]->alias . '/' . $this->data['producer'][0]->alias . '/' . $collection->id . '/' . $collection->alias?>" class="btn btn-danger" role="button">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>