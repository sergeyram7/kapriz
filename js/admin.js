var Producer = {
    removeItem: function(id){
        if (confirm('Удалить запись из базы данных???')) {
            var url = '/admin/function/producer.php';
            var item = document.getElementById('item' + id);
            jQuery.ajax({
                url: url, //Адрес подгружаемой страницы
                type: "GET", //Тип запроса
                dataType: "html", //Тип данных
                //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
                data: "action=" + 'removeProducer' + "&id=" + id,
                success: function (response) { //Если все нормально
                    $('#item' + id).remove();
                },
                error: function (response) { //Если ошибка
                    alert("Ошибка при отправке формы");
                }
            });
        }
    }
};
var Collection = {
    removeItem: function(id){
        if (confirm('Удалить запись из базы данных???')) {
            var url = '/admin/function/collection.php';
            var item = document.getElementById('item' + id);
            jQuery.ajax({
                url: url, //Адрес подгружаемой страницы
                type: "GET", //Тип запроса
                dataType: "html", //Тип данных
                //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
                data: "action=" + 'removeProducer' + "&id=" + id,
                success: function (response) { //Если все нормально
                    $('#item' + id).remove();
                    //$('#debag').html(response);
                },
                error: function (response) { //Если ошибка
                    alert("Ошибка при отправке формы");
                }
            });
        }
    }
};
var Product = {
    addItemId:2,
    addItem: function(){
        var addHtml = '<div class="col-md-6 productItem panel panel-default" id="addItem'+this.addItemId+'">'+
            '<div style="float:right;margin-top:14px;cursor:pointer"><span class="glyphicon glyphicon-remove" onclick="$(\'#addItem'+this.addItemId+'\').remove()"></span></div>'+
            '<div class="panel-body">'+
            '<div class="form-group">'+
            '<label for="product_name">Название</label>'+
            '<input type="text" class="form-control" id="product_name" name="product_name[]" value="">'+
            '</div>'+
            '<div class="form-group">'+
            '<label for="product_width">Ширина</label>'+
            '<input type="text" class="form-control" id="product_width" name="product_width[]" value="">'+
            '</div>'+
            '<div class="form-group">'+
            '<label for="product_height">Высота</label>'+
            '<input type="text" class="form-control" id="product_height" name="product_height[]" value="">'+
            '</div>'+
            '<div class="form-group">'+
            '<label for="product_unit">Единица измерения</label>'+
        '<input type="text" class="form-control" id="product_unit" name="product_unit[]" value="">'+
            '</div>'+
            '<div class="form-group">'+
            '<label for="product_cost">Стоимость</label>'+
            '<input type="text" class="form-control" id="product_cost" name="product_cost[]" value="">'+
            '</div>'+
            '<div class="form-group">'+
            '<label for="product_costOf">Стоимость за...</label>'+
        '<input type="text" class="form-control" id="product_costOf" name="product_costOf[]" value="">'+
            '</div>'+
            '<div class="form-group">'+
            '<label for="product_img">Изображение продукта</label>'+
        '<input type="file" id="product_img" name="product_img[]">'+
            '</div>'+
            '</div>'+
            '</div>';
        $('#product_list').append(addHtml);
        this.addItemId++;
    },
    removeItem: function(itemId){
      if(confirm('Удалить элемент?')){
        var data = "action=removeProduct&id="+itemId;
        $.get('/admin/function/product.php', data, function() {
          $('#productItem'+itemId).remove();
        });
      }
    }
};
var Order = {
    processed: function(id){
        if (confirm('Заказ обработан?')) {
            var url = '/admin/function/order.php';
            jQuery.ajax({
                url: url, //Адрес подгружаемой страницы
                type: "GET", //Тип запроса
                dataType: "html", //Тип данных
                //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
                data: "action=" + 'processed' + "&id=" + id,
                success: function (response) { //Если все нормально
                    $('#status' + id).removeClass('label-danger');
                    $('#status' + id).addClass('label-success');
                    $('#status' + id).html('Обработан');
                    $('#option' + id).remove();
                },
                error: function (response) { //Если ошибка
                    alert("Ошибка при отправке формы");
                }
            });
        }
    }
};
var adminCallMe = {
    processed: function(id){
        if (confirm('Заказ обработан?')) {
            var url = '/admin/function/callme.php';
            jQuery.ajax({
                url: url, //Адрес подгружаемой страницы
                type: "GET", //Тип запроса
                dataType: "html", //Тип данных
                //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
                data: "action=" + 'processed' + "&id=" + id,
                success: function (response) { //Если все нормально
                    $('#statusCallMe' + id).removeClass('label-danger');
                    $('#statusCallMe' + id).addClass('label-success');
                    $('#statusCallMe' + id).html('Обработан');
                    $('#optionCallMe' + id).remove();
                },
                error: function (response) { //Если ошибка
                    alert("Ошибка при отправке формы");
                }
            });
        }
    }
};

var adminReview = {
    removeItem: function(id){
        if (confirm('Удалить запись из базы данных???')) {
            var url = '/admin/function/review.php';
            var item = document.getElementById('itemReview' + id);
            jQuery.ajax({
                url: url, //Адрес подгружаемой страницы
                type: "GET", //Тип запроса
                dataType: "html", //Тип данных
                //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
                data: "action=" + 'remove' + "&id=" + id,
                success: function (response) { //Если все нормально
                    $('#itemReview' + id).remove();
                },
                error: function (response) { //Если ошибка
                    alert("Ошибка при отправке формы");
                }
            });
        }
    },
    publish: function(id){
        if (confirm('Опубликовать запись?')) {
            var url = '/admin/function/review.php';
            jQuery.ajax({
                url: url, //Адрес подгружаемой страницы
                type: "GET", //Тип запроса
                dataType: "html", //Тип данных
                //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
                data: "action=" + 'publish' + "&id=" + id,
                success: function (response) { //Если все нормально
                    $('#optionPublic'+id).attr('title','Снять с публикации');
                    $('#optionPublic'+id).attr('onclick','adminReview.unPublish('+id+')');
                    $('#optionPublic'+id).children('.glyphicon').removeClass('glyphicon-ok');
                    $('#optionPublic'+id).children('.glyphicon').addClass('glyphicon-minus');
                    $('#optionPublic'+id).removeClass('btn-success');
                    $('#optionPublic'+id).addClass('btn-primary');
                    $('#optionPublic'+id).attr('id','optionUnPublic'+id);

                    $('#statusReview'+id).removeClass('label-danger');
                    $('#statusReview'+id).addClass('label-success');
                    $('#statusReview'+id).html('Опубликован');
                },
                error: function (response) { //Если ошибка
                    alert("Ошибка при отправке формы");
                }
            });
        }
    },
    unPublish: function(id){
        if (confirm('Снять запись с публикации?')) {
            var url = '/admin/function/review.php';
            jQuery.ajax({
                url: url, //Адрес подгружаемой страницы
                type: "GET", //Тип запроса
                dataType: "html", //Тип данных
                //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
                data: "action=" + 'unPublish' + "&id=" + id,
                success: function (response) { //Если все нормально
                    $('#optionUnPublic'+id).attr('title','Опубликовать');
                    $('#optionUnPublic'+id).attr('onclick','adminReview.publish('+id+')');
                    $('#optionUnPublic'+id).children('.glyphicon').removeClass('glyphicon-minus');
                    $('#optionUnPublic'+id).children('.glyphicon').addClass('glyphicon-ok');
                    $('#optionUnPublic'+id).removeClass('btn-primary');
                    $('#optionUnPublic'+id).addClass('btn-success');
                    $('#optionUnPublic'+id).attr('id','optionPublic'+id);

                    $('#statusReview'+id).removeClass('label-success');
                    $('#statusReview'+id).addClass('label-danger');
                    $('#statusReview'+id).html('Не опубликован');
                },
                error: function (response) { //Если ошибка
                    alert("Ошибка при отправке формы");
                }
            });
        }
    }
};
$('#name').change(function(){
  var name = $('#name').val();
  $('#alias').val(strToLatin(name));
})

//Транслитерация кириллицы в латиницу
function strToLatin(str) {
	str = str.toLowerCase(); // все в нижний регистр
		var cyr2latChars = new Array(
				['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
				['д', 'd'],  ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
				['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'],
				['м', 'm'],  ['н', 'n'], ['о', 'o'], ['п', 'p'],  ['р', 'r'],
				['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
				['х', 'h'],  ['ц', 'c'], ['ч', 'ch'],['ш', 'sh'], ['щ', 'shch'],
				['ъ', ''],  ['ы', 'y'], ['ь', ''],  ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],

				['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
				['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
				['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
				['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
				['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
				['z', 'z'],

				[' ', '_'],['0', '0'],['1', '1'],['2', '2'],['3', '3'],
				['4', '4'],['5', '5'],['6', '6'],['7', '7'],['8', '8'],['9', '9'],
				['-', '-']

    );

    var newStr = new String();

    for (var i = 0; i < str.length; i++) {

        ch = str.charAt(i);
        var newCh = '';

        for (var j = 0; j < cyr2latChars.length; j++) {
            if (ch == cyr2latChars[j][0]) {
                newCh = cyr2latChars[j][1];
            }
        }
        // Если найдено совпадение, то добавляется соответствие, если нет - пустая строка
        newStr += newCh;

    }
    // Удаляем повторяющие знаки - Именно на них заменяются пробелы.
    // Так же удаляем символы перевода строки, но это наверное уже лишнее
    return newStr.replace(/[_]{2,}/gim, '_').replace(/\n/gim, '');
}
