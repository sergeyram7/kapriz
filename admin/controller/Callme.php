<?php

namespace Admin\Controller;
use Admin\Model\Callme as CallmeModel;
use Admin\Model\Category;
use Admin\Model\Orders;
use Admin\Model\OrderProduct;
use Admin\Model\Collection;
use Admin\Model\Product;
use Admin\Model\Producer;
use Admin\View\View;

class Callme
{
    public function action_all($where=[]){
        $callme = CallmeModel::getAll([],['id'=>'DESC']);
        $view = new View();
        $view->assign('callme',$callme);
        $view->assign('pageTitle', 'Обратный звонок :: ');
        $view->assign('display', 'callme/default.php');
        $view->template();
    }
}