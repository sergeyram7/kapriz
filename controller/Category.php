<?php

namespace App\Controller;
use App\Model\Category as CategoryModel;
use App\Model\Collection;
use App\Model\Producer;
use App\Model\Product;
use App\View\View;

class Category{
//    public function action_all($where=[]){
//        if(empty($where)) $where=[];
//        $category = Category::getAll();
//        $view = new View();
//        $view->assign('category',$category );
//        $view->assign('pageTitle', 'Категории :: ');
//        $view->assign('display', 'category/list.php');
//        $view->template();
//    }
    public function action_view(){
        if (!empty($_GET['alias'])) {
            $category = CategoryModel::getSome(1, ['alias' => $_GET['alias']], []);
            if(empty($category)) $this->error404();
            $producerList = Producer::getRow([],[],['category_id' => $category[0]->id]);
            $collection = Collection::getAll(['category_id' => $category[0]->id]);
            $countProduct = false;
            $producerItem = false;

            for($i=0;isset($collection[$i]);$i++){
                $countProduct[$i] = Product::getNumAll(['collection_id'=>$collection[$i]->id]);
            }
            for($i=0;isset($collection[$i]);$i++){
                $producerItem[$i] = Producer::getSome(1,['id'=>$collection[$i]->producer_id],[])[0];
            }
            $view = new View();
            $view->assign('category', $category);
            $view->assign('producerList', $producerList);
            if ($producerItem) {
                $view->assign('producerItem', $producerItem);
            }
            $view->assign('collection', $collection);
            if ($countProduct) {
                $view->assign('countProduct', $countProduct);
            }
            $view->assign('pageTitle', $category[0]->title);
            $view->assign('keywords', $category[0]->metaKeywords);
            $view->assign('description', $category[0]->metaDescription);
            $view->assign('display', 'category/default.php');
            $view->template();
        } else if(!empty($_GET['category'])&&!empty($_GET['producer'])&&empty($_GET['id_collection'])) {
            $category = CategoryModel::getSome(1, ['alias' => $_GET['category']], []);
            $producer = Producer::getSome(1, ['alias' => $_GET['producer']], []);
            if(empty($producer)) $this->error404();
            $collection = Collection::getAll(['category_id' => $category[0]->id,'producer_id' => $producer[0]->id]);
            for($i=0;isset($collection[$i]);$i++){
                $countProduct[$i] = Product::getNumAll(['collection_id'=>$collection[$i]->id]);
            }
            $view = new View();
            $view->assign('category', $category);
            $view->assign('producer', $producer);
            $view->assign('collection', $collection);
            $view->assign('countProduct',$countProduct);
            $view->assign('pageTitle', $category[0]->name . ' от "' . $producer[0]->name);
            $view->assign('display', 'category/producer.php');
            $view->template();
        } else if($_GET['category']&&$_GET['producer']&&$_GET['id_collection']) {
            $category = CategoryModel::getSome(1, ['alias' => $_GET['category']], []);
            $producer = Producer::getSome(1, ['alias' => $_GET['producer']], []);
            $collection = Collection::getSome(1, ['id' => $_GET['id_collection']], []);
            if(empty($collection)) $this->error404();
            $product = Product::getAll(['collection_id' => $collection[0]->id]);
            $view = new View();
            $view->assign('category', $category);
            $view->assign('producer', $producer);
            $view->assign('collection', $collection);
            $view->assign('product', $product);
            $view->assign('pageTitle', $collection[0]->name);
            $view->assign('display', 'collection/default.php');
            $view->template();
        } else {
            $this->error404();
        }
    }
    private function error404(){
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        include_once __DIR__ .'/../404.html';
        die();
    }
}