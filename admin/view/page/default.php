<div class="row">
    <div class="col-md-12">
        <h2>Все страницы</h2>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#id</th>
                <th>Название</th>
                <th>Дата</th>
                <th>Алиас</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['page'] as $key => $item){ ?>
                <tr>
                    <td>
                        <?php echo $item->id ?>
                    </td>
                    <td>
                        <?php echo $item->name ?>
                    </td>
                    <td>
                        <?php echo $item->date ?>
                    </td>
                    <td>
                        <?php echo $item->alias ?>
                    </td>
                    <td>
                        <a href="/admin/page/edit/<?php echo $item->id ?>" class="btn btn-default">Редактировать</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>