<?php
    header('Content-Type: text/html; charset=utf-8');

    session_start();

    ini_set("display_errors",1);
    error_reporting(E_ALL);

    require_once __DIR__ . '/autoload.php';

    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $pathParts = explode('/', $path);

if(!empty($pathParts[2])){
    $ctrl = 'Admin\Controller\\'.ucfirst($pathParts[2]);
} else {
    $ctrl = 'Admin\Controller\Index';
}

    if(!empty($pathParts[3])){
        $action = 'action_'.ucfirst($pathParts[3]);
    } else {
        $action = 'action_all';
    }
//    var_dump($ctrl);
//    var_dump($action);
$auth = new Admin\Controller\AuthClass;
if ($auth->isAuth()) { // Если пользователь авторизован, приветствуем:
    $controller=new $ctrl;
    $controller->$action();
    echo "<br/><br/><a href='?is_exit=1'>Выйти</a>"; //Показываем кнопку выхода
}
else { //Если не авторизован, показываем форму ввода логина и пароля
?>
<form method="post" action="">
    Логин: <input type="text" name="login"
                  value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>" />
    <br/>
    Пароль: <input type="password" name="password" value="" /><br/>
    <input type="submit" value="Войти" />
</form>
    <?php
}
?>
