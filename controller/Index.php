<?php

namespace App\Controller;
use App\Model\Category;
use App\Model\Page;
use App\Model\Collection;
use App\Model\Product;
use App\Model\Producer;
use App\View\View;

class Index
{
    public function action_all($where=[]){
        if(empty($where)) $where=[];
        $page = Page::getSome(1,['alias'=>'index'],[]);
        $collection = Collection::getSome(4,['isRecommended'=>'1'],[]);
        for($i=0;isset($collection[$i]);$i++){
            $countProduct[$i] = Product::getNumAll(['collection_id'=>$collection[$i]->id]);
        }
//        $countProduct = Product::getNumAll(['collection_id'=>$collection[0]->id]);
        for($i=0;isset($collection[$i]);$i++){
            $producer[$i] = Producer::getSome(1,['id'=>$collection[$i]->producer_id],[])[0];
        }
        //$producer = Producer::getSome(1,['id'=>$collection[0]->producer_id],[]);
        for($i=0;isset($collection[$i]);$i++){
            $category[$i] = Category::getSome(1,['id'=>$collection[$i]->category_id],[])[0];
        }
        $view = new View();
        $view->assign('page',$page);
        $view->assign('collection',$collection);
        $view->assign('countProduct',$countProduct);
        $view->assign('producer',$producer);
        $view->assign('category',$category);
        $view->assign('pageTitle', $page[0]->name);
        $view->assign('keywords', 'Керамическая плитка, мозаика, керамогранит, клинкер, плитка для пола, плитка для ванной, облицовочный камень, Купить керамическую плитку');
        $view->assign('description', 'Это широчайший ассортимент высококачественных отделочных материалов. Представлены коллекции керамической плитки и несомненно ВЫГОДНЫЕ ЦЕНЫ!');
        $view->assign('display', 'page/index.php');
        $view->template();
    }
    public function action_page(){
        if ($_GET['alias']) {
            $page = Page::getSome(1, ['alias' => $_GET['alias']], []);
            if(empty($page)) $this->error404();
            $view = new View();
            $view->assign('page', $page);
            $view->assign('pageTitle', $page[0]->name);
            $view->assign('display', 'page/page.php');
            $view->template();
        } else {
            $this->error404();
        }
    }
    private function error404(){
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        include_once __DIR__ .'/../404.html';
        die();
    }
}