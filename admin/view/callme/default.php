<h1 class="page-header">Обратный звонок</h1>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#id</th>
                <th>Дата</th>
                <th>Телефон</th>
                <th>Статус</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['callme'] as $key => $callme){ ?>
                <tr>
                    <td>
                        <?php echo $callme->id ?>
                    </td>
                    <td>
                        <?php echo $callme->date ?>
                    </td>
                    <td>
                        <?php echo $callme->phone?>
                    </td>
                    <td>
                        <?php if($callme->status){
                            echo '<span class="label label-success" id="statusCallMe'.$callme->id.'">Обработан</span>';
                        } else{
                            echo '<span class="label label-danger" id="statusCallMe'.$callme->id.'">Не обработан</span>';
                        }?>
                    </td>
                    <td>
                        <?php if(!$callme->status){?>
                            <button type="button" class="btn btn-success" title="Заказ обработан" id="optionCallMe<?php echo $callme->id?>" onclick="adminCallMe.processed(<?php echo $callme->id ?>)"><span class="glyphicon glyphicon-ok"></span></button>
                        <?php }?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>