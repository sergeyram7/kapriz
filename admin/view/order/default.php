<h1 class="page-header">Заказы</h1>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#id</th>
                <th>ФИО</th>
                <th>Телефон</th>
                <th>Email</th>
                <th>Адрес</th>
                <th>Дата и время</th>
                <th>Способ оплаты</th>
                <th>Статус</th>
                <th>Общая стоимость</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['orders'] as $key => $order){ ?>
                <tr>
                    <td>
                        <?php echo $order->id ?>
                    </td>
                    <td>
                        <?php echo $order->userName ?>
                    </td>
                    <td>
                        <?php echo $order->phone?>
                    </td>
                    <td>
                        <?php echo $order->email?>
                    </td>
                    <td>
                        <?php echo $order->address?>
                    </td>
                    <td>
                        <?php echo $order->dateTime?>
                    </td>
                    <td>
                        <?php if($order->payMethod == 'cash') {
                            echo 'Наличные';
                        } else {
                            echo 'Безналичные';
                        }?>
                    </td>
                    <td>
                        <?php if($order->status){
                            echo '<span class="label label-success" id="status'.$order->id.'">Обработан</span>';
                        } else{
                            echo '<span class="label label-danger" id="status'.$order->id.'">Не обработан</span>';
                        }?>
                    </td>
                    <td>
                        <?php echo $order->orderProductCost?>
                    </td>
                    <td>
                        <?php if(!$order->status){?>
                            <button type="button" class="btn btn-success" title="Заказ обработан" id="option<?php echo $order->id?>" onclick="Order.processed(<?php echo $order->id ?>)"><span class="glyphicon glyphicon-ok"></span></button>
                        <?php }?>
                        <a href="/admin/order/view/<?php echo $order->id?>" class="btn btn-default">Просмотр</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>