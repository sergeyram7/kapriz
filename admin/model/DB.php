<?php

namespace Admin\Model;

require_once __DIR__ . '/DBConfig.php';

class DB
{
    private $dbh;
    private $className='stdClass';
    public function __construct()
    {
        $this->dbh = new \PDO('mysql:dbname='.DB_NAME.';host='. DB_HOST .'',DB_LOGIN,DB_PASS);
        $this->query('SET NAMES \'utf8\'');
    }

    public function setClassName($className)
    {
        $this->className = $className;
    }

    public function query($sql,$params=[]){
        $sth = $this->dbh->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(\PDO::FETCH_CLASS,$this->className);
    }
    public function querySend($sql,$params=[]){
        $sth = $this->dbh->prepare($sql);
        $sth->execute($params);
        return $this->dbh->lastInsertId();
    }
    public function queryArray($sql,$params=[]){
        $sth = $this->dbh->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll();
    }
}