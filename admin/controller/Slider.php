<?php

namespace Admin\Controller;
use Admin\View\View;

class Slider
{
    public function action_all(){
        if($_POST){
            //Читаем и записываем
            $str = file_get_contents(__DIR__ . '/../../setting.json');
            $activeSlider = json_decode($str, true);
            $activeSlider['slider']['status']=$_POST['status'];
            $activeSlider['slider']['select']=(int)$_POST['selectSlide'];
            file_put_contents(__DIR__ . '/../../setting.json', json_encode($activeSlider));
            //читаем обновленный файл
            $str = file_get_contents(__DIR__ . '/../../setting.json');
            $activeSlider = json_decode($str, true);
            $view = new View();
            $view->assign('slider',$activeSlider['slider']);
            $view->assign('pageTitle', 'Слайдер :: ');
            $view->assign('display', 'slider/default.php');
            $view->template();
        } else {
            $str = file_get_contents(__DIR__ . '/../../setting.json');
            $activeSlider = json_decode($str, true);
            $view = new View();
            $view->assign('slider',$activeSlider['slider']);
            $view->assign('pageTitle', 'Слайдер :: ');
            $view->assign('display', 'slider/default.php');
            $view->template();
        }
    }
}