<?php
header('Content-Type: text/html; charset=utf-8');

session_start();

require_once '../model/DBConfig.php';

if($_GET['action']=='callme'){
    $mysqli = new mysqli(DB_HOST, DB_LOGIN, DB_PASS, DB_NAME);
    $sql = 'INSERT INTO '.$_GET['action'].' SET phone = '.$_GET['phone'];
    $res = $mysqli->query($sql);
}
if($_GET['action']=='reviews'){
    $mysqli = new mysqli(DB_HOST, DB_LOGIN, DB_PASS, DB_NAME);
    $mysqli->query('SET NAMES \'utf8\'');
    $sql = 'INSERT INTO '.$_GET['action'].' SET userName = \''.$_GET['userName'].'\', text = \''.$_GET['text'].'\'';
    $res = $mysqli->query($sql);
}
if($_GET['action']=='orders') {
    session_regenerate_id();
    $mysqli = new mysqli(DB_HOST, DB_LOGIN, DB_PASS, DB_NAME);
    $mysqli->query('SET NAMES \'utf8\'');
    $mysqli->query('SET CHARACTER SET \'utf8\'');
    $sql = 'INSERT INTO '.$_GET['action'].' SET userName = \''.$_GET['userName'].'\', phone = \''.$_GET['phone'].'\'';
    $sql .= ', email = \''.$_GET['email'].'\'';
    $sql .= ', address = \''.$_GET['address'].'\'';
    $sql .= ', payMethod = \''.$_GET['payMethod'].'\'';
    $totalCost = 0;
    foreach($_SESSION['cart']['cost'] as $key => $cost){
        $totalCost += $_SESSION['cart']['cost'][$key]*$_SESSION['cart']['num'][$key];
    }
    $sql .= ', orderProductCost = \''.$totalCost.'\'';
    $res = $mysqli->query($sql);
    $lastId = $mysqli->insert_id;
    foreach($_SESSION['cart']['id'] as $key => $id){
        $sql = 'INSERT INTO orderProduct SET product_id = \''.$id.'\', num = \''.$_SESSION['cart']['num'][$key].'\', cost = \''.$_SESSION['cart']['cost'][$key].'\', order_id = \''.$lastId.'\'';
        $res = $mysqli->query($sql);
    }
    $finish = 'yup';
    echo $lastId;
    session_destroy();
}