<?php
    $path = $this->rootPath . '/reviews/view/';
?>
<?php foreach($this->data['reviews'] as $reviews):?>
<div class="blockTextReview">
    <div class="row">
        <div class="col-md-5">
            <a href="<?php echo $path?><?php echo $reviews->id?>"><img src="/img/textreviews/<?php echo $reviews->urlPhoto?>" alt="<?php echo $reviews->title?>" class="img-thumbnail" width="100%"></a>
        </div>
        <div class="col-md-7">
            <div><a href="<?php echo $path?><?php echo $reviews->id?>"><?php echo $reviews->title?></a></div>
            <div><b><?php echo $reviews->dateTime;?></b></div>
            <p><?php echo $reviews->shortTextReviews?></p>
        </div>
    </div>
    <a href="<?php echo $path?><?php echo $reviews->id?>" class="btn btn-default">Читать полностью</a>
</div>
<?php endforeach;?>