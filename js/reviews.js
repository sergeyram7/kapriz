var Reviews = {
    url: '/function/send.php',
    send: function(){
        var userName = document.getElementById('reviewName').value;
        var text = document.getElementById('reviewText').value;
        jQuery.ajax({
            url:     this.url, //Адрес подгружаемой страницы
            type:     "GET", //Тип запроса
            dataType: "html", //Тип данных
            data: "action="+'reviews'+"&userName="+userName+"&text="+text,
            success: function(response) { //Если все нормально
                $('#alert-review').addClass("alert-success");
                var seccessText = '<p><strong>Спасибо!</strong> Отзыв был принят, после проверки, мы его добавим.</p>';
                $('#alert-review').html(seccessText);
                $('#alert-review').fadeIn();
            },
            error: function(response) { //Если ошибка
                $('#alert-review').addClass("alert-danger");
                var dangerText = '<p><strong>Ошибка!</strong> Пожалуйста, свяжитесь с администратором</p>';
                $('#alert-review').html(dangerText);
                $('#alert-review').fadeIn();
            }
        });
    },
};