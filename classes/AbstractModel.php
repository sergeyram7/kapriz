<?php

use App\Model\DB;

//require_once __DIR__ . '/../function/getFileViewPath.php';

abstract class AbstractModel
{
    protected static $table;

    private static function where($where){
        $sql='';
        if(!empty($where)){
            foreach ($where as $key=>$value){
                if(is_array($value)){
                    $selectArea='';
                    foreach ($value as $valueArr){
                        $selectArea.="'".$valueArr."',";
                    }
                    $sql.=" AND ".$key." IN (".substr($selectArea, 0, -1).") ";
                } else {
                    $sql .= 'AND '. $key . '=\'' . $value . '\' ';
                }
            }
            return $sql;
        } else return '';
    }

    private static function like($like){
        $sql='';
        if(!empty($like)){
            foreach ($like as $key=>$value){
                $sql .= 'AND '. $key . ' LIKE \'%' . $value . '%\' ';
            }
            return $sql;
        } else return '';
    }

    private static function set($parameters){
        $sql='';
        if(!empty($parameters)){
            foreach ($parameters as $key=>$value){
                $sql .= ','. $key . '=\'' . $value . '\'';
            }
            return substr($sql,1);
        } else return '';
    }

    /*private static function multiWhere($mWhere){
        $sql='';
        if(!empty($mWhere)){
            foreach ($mWhere as $key=>$value){
                $sql .= 'AND '. $key . '=' . $value . ' ';
            }
            return $sql;
        } else return '';
    }*/

    private static function sorting($sorting){
        if(!empty($sorting)){
            return 'ORDER BY ' . key($sorting) . ' ' . $sorting[key($sorting)];
        } else return '';
    }

    public static function getAll($where=[],$sorting=[]){
        $class=get_called_class();
        $db = new DB;
        $db->setClassName($class);
        $sql = 'SELECT * FROM ' . static::$table . ' WHERE 1 ';
        $sql .= self::where($where);
        $sql .= self::sorting($sorting);
        //var_dump($sql);
        return $db->query($sql);
    }

    public static function getSome($num,$where=[],$sorting=[]){
        $class=get_called_class();
        $db = new DB;
        $db->setClassName($class);
        $sql = 'SELECT * FROM ' . static::$table . ' WHERE 1 ';
        $sql .= self::where($where);
        $sql .= self::sorting($sorting);
        $sql .= ' LIMIT ' . (int)$num;
        //var_dump($sql);
        return $db->query($sql);
    }

    public static function getRow($where=[],$sorting=[],$like=[]){
        $class=get_called_class();
        $db = new DB;
        $db->setClassName($class);
        $sql = 'SELECT * FROM ' . static::$table . ' WHERE 1 ';
        $sql .= self::where($where);
        $sql .= self::like($like);
        $sql .= self::sorting($sorting);
        //$sql .= ' LIMIT ' . $num['from'] . ', ' . $num['num'];
        $sql = str_replace('$  ', ' ',$sql);
        //var_dump($sql);
        return $db->query($sql);
    }

    public static function getOne($id){
        $db = new DB;
        return $db->query('SELECT * FROM ' . static::$table . ' WHERE id=:id',[':id'=>(int)$id]);
    }

    public static function getBy($column, $value){
        $db = new DB;
        $sql="SELECT * FROM " . static::$table . " WHERE $column='$value'";
        return $db->query($sql)[0];
    }

    public static function getNumAll($where=[]){
        $class=get_called_class();
        $db = new DB;
        $db->setClassName($class);
        $sql = 'SELECT COUNT(*) FROM ' . static::$table . ' WHERE 1 ';
        $sql .= self::where($where);
        //var_dump($sql);
        $res=$db->queryArray($sql);
        return $res[0][0];
    }
    public static function sendAjax($parameters=[]){
        $class=get_called_class();
        $db = new DB;
        $db->setClassName($class);
        $sql = 'INSERT INTO '.static::$table.' SET ';
        $sql .= self::set($parameters);
        //var_dump($sql);
        $res=$db->querySend($sql);
        return $res;
    }
}