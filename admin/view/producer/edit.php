<?php $item = $this->data['producer'][0];
if(!empty($this->data['save'])){
    echo '<div class="alert alert-success"><strong>Сохранено</strong></div>';
}
?>
<div class="row">
    <div class="col-md-12">
        <h2>Редактирование производителя: <?php echo $item->name?></h2>
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Имя:</label>
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $item->name?>">
            </div>
            <div class="form-group">
                <label for="alias">Алиас (формируется сам, после имени):</label>
                <input type="text" class="form-control" id="alias" name="alias" value="<?php echo $item->alias?>">
            </div>

            <div class="form-group">
                <label for="summernote">Описание:</label>
                <textarea class="form-control" id="summernote" name="description" rows="8"><?php echo $item->description?></textarea>
            </div>
            <div class="checkbox">
                <?php foreach($this->data['category'] as $category){?>
                    <label>
                        <input type="checkbox" value="<?php echo $category->id?>" name="category_id[]" <?php $arrSearch = array_search($category->id, $this->data['selectCategory']); if($arrSearch||$arrSearch===0) echo 'checked' ?>> <?php echo $category->name?>
                    </label>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="img">Логотип производителя</label>
                <?php if(($item->img)){?>
                    <span class="glyphicon glyphicon-ok" style="color: darkgreen"></span><br>
                    <img src="<?php echo '/img/producer/'.$item->img?>" alt="" width="200">
                <?php }?>
                <input type="file" id="img" name="img" value="/<?php echo $item->img?>">
            </div>
            <div class="form-group">
                <label for="metaKeywords">Ключевые слова:</label>
                <input type="text" class="form-control" id="metaKeywords" name="metaKeywords" value="<?php echo $item->metaKeywords?>">
            </div>
            <div class="form-group">
                <label for="metaDescription">Description:</label>
                <input type="text" class="form-control" id="metaDescription" name="metaDescription" value="<?php echo $item->metaDescription?>">
            </div>
            <button type="submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
</div>