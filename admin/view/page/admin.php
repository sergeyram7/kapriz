<h1 class="page-header">Главная</h1>
<div class="row">
    <div class="col-md-12">
        <h2>Обратный звонок</h2>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#id</th>
                <th>Дата</th>
                <th>Телефон</th>
                <th>Статус</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['callme'] as $key => $callme){ ?>
                <tr>
                    <td>
                        <?php echo $callme->id ?>
                    </td>
                    <td>
                        <?php echo $callme->date ?>
                    </td>
                    <td>
                        <?php echo $callme->phone?>
                    </td>
                    <td>
                        <?php if(!empty($callme->status)){
                            echo '<span class="label label-success" id="statusCallMe'.$callme->id.'">Обработан</span>';
                        } else{
                            echo '<span class="label label-danger" id="statusCallMe'.$callme->id.'">Не обработан</span>';
                        }?>
                    </td>
                    <td>
                        <?php if(empty($callme->status)){?>
                            <button type="button" class="btn btn-success" title="Заказ обработан" id="optionCallMe<?php echo $callme->id?>" onclick="adminCallMe.processed(<?php echo $callme->id ?>)"><span class="glyphicon glyphicon-ok"></span></button>
                        <?php }?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <h2>Последние коллекции</h2>
        <a href="/admin/collection/add" class="btn btn-success">+ Добавить</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#id</th>
                <th>Название</th>
                <th>Описание</th>
                <th>Категория</th>
                <th>Производитель</th>
                <th>Позиций</th>
                <th>Рекоменд.</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['collection'] as $key => $collection){ ?>
                <tr>
                    <td>
                        <?php echo $collection->id ?>
                    </td>
                    <td>
                        <?php echo $collection->name ?>
                    </td>
                    <td>
                        <?php echo substr($collection->short_description, 0, 20)?>
                    </td>
                    <td>
                        <?php echo $this->data['categoryName'][$key][0]->name?>
                    </td>
                    <td>
                        <?php echo $this->data['producerName'][$key][0]->name?>
                    </td>
                    <td>
                        <?php echo $this->data['countProduct'][$key]?>
                    </td>
                    <td>
                        <?php if($collection->isRecommended){
                            echo '+';
                        } else{
                            echo '-';
                        }?>
                    </td>
                    <td>
                        <a href="/admin/collection/edit/<?php echo $collection->id ?>" class="btn btn-default">Редактировать</a>
                        <button type="button" onclick="Producer.removeItem(<?php echo $collection->id ?>)" class="btn btn-danger">Удалить</button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-12">
        <h2>Последние заказы</h2>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>#id</th>
                <th>ФИО</th>
                <th>Телефон</th>
                <th>Email</th>
                <th>Адрес</th>
                <th>Дата и время</th>
                <th>Способ оплаты</th>
                <th>Статус</th>
                <th>Общая стоимость</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['orders'] as $key => $order){ ?>
                <tr>
                    <td>
                        <?php echo $order->id ?>
                    </td>
                    <td>
                        <?php echo $order->userName ?>
                    </td>
                    <td>
                        <?php echo $order->phone?>
                    </td>
                    <td>
                        <?php echo $order->email?>
                    </td>
                    <td>
                        <?php echo $order->address?>
                    </td>
                    <td>
                        <?php echo $order->dateTime?>
                    </td>
                    <td>
                        <?php if($order->payMethod == 'cash') {
                            echo 'Наличные';
                        } else {
                            echo 'Безналичные';
                        }?>
                    </td>
                    <td>
                        <?php if($order->status){
                            echo '<span class="label label-success" id="status'.$order->id.'">Обработан</span>';
                        } else{
                            echo '<span class="label label-danger" id="status'.$order->id.'">Не обработан</span>';
                        }?>
                    </td>
                    <td>
                        <?php echo $order->orderProductCost?>
                    </td>
                    <td>
                        <?php if(!$order->status){?>
                            <button type="button" class="btn btn-success" title="Заказ обработан" id="option<?php echo $order->id?>" onclick="Order.processed(<?php echo $order->id ?>)"><span class="glyphicon glyphicon-ok"></span></button>
                        <?php }?>
                        <a href="/admin/order/view/<?php echo $order->id?>" class="btn btn-default">Просмотр</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>