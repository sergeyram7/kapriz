<?php

namespace Admin\Controller;
use Admin\Model\Category;
use Admin\Model\Producer as ProducerModel;
use Admin\View\View;

class Producer{
    public function action_all(){
        $producer = ProducerModel::getAll();
        $view = new View();
        $view->assign('producer', $producer);
        $view->assign('pageTitle', 'Производители' . ' :: ');
        $view->assign('display', 'producer/default.php');
        $view->template();
    }
    public function action_edit(){
        if($_POST){
            //обработка category_id
            $updateCategory = $_POST['category_id'];
            $updateCategory = implode(",", $updateCategory);
            //обработка изображения
            $this->uploadImg();
            $parameters = ['name'=>$_POST['name'],'alias'=>$_POST['alias'],'description'=>str_replace("'","&rsquo;",$_POST['description']),'metaKeywords'=>$_POST['metaKeywords'],'metaDescription'=>$_POST['metaDescription'],'category_id'=>$updateCategory];
            if ($_FILES['img']['name']) {
                $parameters['img'] = $_FILES['img']['name'];
            }
            //var_dump($parameters);
            ProducerModel::update($parameters,['id'=>$_GET['id']]);
            $producer = ProducerModel::getSome(1,['id'=>$_GET['id']]);
            $categoryList = explode(',', $producer[0]->category_id);
            $tempCategory = [];
            $selectCategory = [];
            for($i=0;isset($categoryList[$i]); $i++){
                $tempCategory[$i] = Category::getSome(1,['id'=>$categoryList[$i]])[0];
                $selectCategory[$i] = $tempCategory[$i]->id;
            }
            $category = Category::getAll();
            $view = new View();
            $view->assign('producer', $producer);
            $view->assign('selectCategory', $selectCategory);
            $view->assign('category', $category);
            $view->assign('save', true);
            $view->assign('pageTitle', 'Редактирование производителя:' .$producer[0]->name. ' :: ');
            $view->assign('display', 'producer/edit.php');
            $view->template();
        } else{
            $producer = ProducerModel::getSome(1,['id'=>$_GET['id']]);
            $categoryList = explode(',', $producer[0]->category_id);
            $tempCategory = [];
            $selectCategory = [];
            for($i=0;isset($categoryList[$i]); $i++){
                $tempCategory[$i] = Category::getSome(1,['id'=>$categoryList[$i]])[0];
                $selectCategory[$i] = $tempCategory[$i]->id;
            }
            $category = Category::getAll();
            $view = new View();
            $view->assign('producer', $producer);
            $view->assign('selectCategory', $selectCategory);
            $view->assign('category', $category);
            $view->assign('pageTitle', 'Редактирование производителя:' .$producer[0]->name. ' :: ');
            $view->assign('display', 'producer/edit.php');
            $view->template();
        }
    }
    public function action_add(){
        if($_POST){
            //обработка category_id
            $updateCategory = $_POST['category_id'];
            $updateCategory = implode(",", $updateCategory);
            //обработка изображения
            $this->uploadImg();
            $parameters = ['name'=>$_POST['name'],'alias'=>$_POST['alias'],'description'=>str_replace("'","&rsquo;",$_POST['description']),'metaKeywords'=>$_POST['metaKeywords'],'metaDescription'=>$_POST['metaDescription'],'category_id'=>$updateCategory];
            if ($_FILES['img']['name']) {
                $parameters['img'] = $_FILES['img']['name'];
            }
            //var_dump($parameters);
            $lastId = ProducerModel::add($parameters);
            //var_dump($lastId);
            $view = new View();
            $view->assign('save', true);
            $view->assign('pageTitle', 'Производитель добавлен :: ');
            $view->assign('display', 'producer/addSuccess.php');
            $view->template();
        } else {
//            $producer = ProducerModel::getSome(1,['id'=>$_GET['id']]);
//            $categoryList = explode(',', $producer[0]->category_id);
//            $tempCategory = [];
//            $selectCategory = [];
//            for($i=0;isset($categoryList[$i]); $i++){
//                $tempCategory[$i] = Category::getSome(1,['id'=>$categoryList[$i]])[0];
//                $selectCategory[$i] = $tempCategory[$i]->id;
//            }
            $category = Category::getAll();
            $view = new View();
//            $view->assign('producer', $producer);
//            $view->assign('selectCategory', $selectCategory);
            $view->assign('category', $category);
            $view->assign('pageTitle', 'Добавить производителя :: ');
            $view->assign('display', 'producer/add.php');
            $view->template();
        }
    }
    private function uploadImg(){
        $uploaddir = __DIR__ . '/../../img/producer/';
        $uploadfile = $uploaddir . basename($_FILES['img']['name']);
        move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile);
//        if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {
//            echo "Файл корректен и был успешно загружен.\n";
//        } else {
//            echo "Возможная атака с помощью файловой загрузки!\n";
//        }
    }
}