<div class="row">
    <div class="col-xs-12">
        <h1 class="underline-header">Ваша корзина</h1>
        <div class="row" id="cart-list">
            <?php
            if(!count($_SESSION['cart']['id'])){?>
                <div class="alert alert-info">
                    <p><strong>Ваша корзина пуста</strong> Вы ещё ничего не добавили в корзину</p>
                </div>
            <?php } else {?>
                <?php $collection = $this->data['collection'][0]?>
                <div class="col-md-8">
                    <h2>Список товаров</h2>
                    <?php //var_dump($this->data['product'])?>
                    <?php //var_dump($this->data['collection'])?>
                    <?php //var_dump($_SESSION)?>
                    <?php //var_dump($_SESSION['cart']['id'][1])?>
                    <div id="cart-list">
                    <?php foreach($this->data['product'] as $key => $product){?>
                        <?php
                            $keySession = array_search($product->id, $_SESSION['cart']['id']);
                        ?>
                        <div id="item<?php echo $product->id?>" class="media" style="padding-bottom:15px;border-bottom: 1px solid #ccc">
                            <div class="pull-left text-center">
                                <img class="media-object" src="/img/product/<?php echo $product->id . '/' . $product->img?>" alt="<?php echo $product->name?>">
                                <div style="margin-top: 5px;margin-right: 10px;margin-left: 10px"><button type="button" class="btn btn-default btn-sm" style="width:100%" onclick="Cart.removeItem(<?php echo $product->id?>,<?php echo $product->cost?>,<?php echo $keySession?>)"><span class="glyphicon glyphicon-remove"></span> Удалить</button></div>
                            </div>
                            <div class="media-body">
                                <small><a href="/category/view/<?php echo $this->data['category'][$key]->alias . '/' . $this->data['producer'][$key]->alias . '/' . $this->data['collection'][$key]->id . '/' . $this->data['collection'][$key]->alias?>"><?php echo $this->data['collection'][$key]->name?></a></small>
                                <h4 class="media-heading"><?php echo $product->name?></h4>
                                <span>Количество:</span>
                                <div class="input-group spinner">
                                    <input id="spinner<?php echo $product->id?>" type="text" class="form-control" value="<?php echo ($_SESSION['cart']['num'][$keySession])?>">
                                    <div class="input-group-btn-vertical">
                                        <button class="btn btn-default dropup" type="button" onclick="Cart.numChange('up',<?php echo $product->id?>,<?php echo $product->cost?>,<?php echo $keySession?>)"><i class="caret" style="margin-top: 6px"></i></button>
                                        <button class="btn btn-default" type="button" onclick="Cart.numChange('down',<?php echo $product->id?>,<?php echo $product->cost?>,<?php echo $keySession?>)"><i class="caret" style="margin-top: 6px"></i></button>
                                    </div>
                                </div>
                                <p style="color: darkgreen"><?php echo $product->cost?> руб. / <?php echo $product->costOf?></p>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                    <p style="margin: 15px;font-size: 14pt"><strong>Итого: </strong><span id="total-cost">
                        <?php if (isset($_SESSION['cart']['cost'])) {
                            $totalCost = 0;
                            foreach($_SESSION['cart']['cost'] as $key => $cost){
                                $totalCost += $cost*$_SESSION['cart']['num'][$key];
                            }
                            echo $totalCost;
                        } ?>
                        </span> руб.</p>
                </div>
                <div class="col-md-4">
                    <h2>Оформить заказ</h2>
                    <form role="form" onsubmit="Cart.send();return false">
                        <div class="form-group">
                            <label for="inputName">ФИО*</label>
                            <input type="text" class="form-control" id="inputName" required="required" placeholder="Иванов Иван Иванович">
                        </div>
                        <div class="form-group">
                            <label for="inputPhone">Телефон*</label>
                            <input type="text" class="form-control" id="inputPhone" required="required" placeholder="+7">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Email</label>
                            <input type="email" class="form-control" id="inputEmail" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Адрес*</label>
                            <input type="text" class="form-control" id="inputAddress" required="required" placeholder="г. Саратов, ул. Московская...">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="radio" name="checkPayment" checked value="cash"> Оплата наличными
                            </label>
                        </div>
                        <button type="submit" class="btn btn-danger">Оформить заказ</button>
                    </form>
                </div>
            <?php } ?>
        </div>
    </div>
</div>