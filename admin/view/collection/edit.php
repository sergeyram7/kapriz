<?php $item = $this->data['collection'];
if (!empty($this->data['product'])) {
    $product = $this->data['product'];
}
if(!empty($this->data['save'])){
    echo '<div class="alert alert-success"><strong>Сохранено</strong></div>';
}
?>
<div class="row">
    <div class="col-md-12">
        <h2>Редактирование коллекции: <?php echo $item->name?></h2>
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Имя:</label>
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $item->name?>">
            </div>
            <div class="form-group">
                <label for="alias">Алиас (формируется сам, после имени):</label>
                <input type="text" class="form-control" id="alias" name="alias" value="<?php echo $item->alias?>">
            </div>
            <div class="form-group">
                <label for="isRecommended">Рекомендуемая коллекция:</label>
                <p><input name="isRecommended" type="radio" value="1" <?php if($item->isRecommended == 1) echo 'checked'?>> Да</p>
                <p><input name="isRecommended" type="radio" value="0" <?php if($item->isRecommended == 0) echo 'checked'?>> Нет</p>
            </div>
            <div class="form-group">
                <label for="category">Категория:</label>
                <select class="form-control" id="category" name="category_id">
                    <?php foreach($this->data['category'] as $category){?>
                        <option value="<?php echo $category->id?>" <?php if($category->id==$item->category_id) echo 'selected' ?>><?php echo $category->name?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="producer">Производитель:</label>
                <select class="form-control" id="producer" name="producer_id">
                    <?php foreach($this->data['producer'] as $producer){?>
                        <option value="<?php echo $producer->id?>" <?php if($producer->id==$item->producer_id) echo 'selected' ?>><?php echo $producer->name?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="summernote">Краткое описание:</label>
                <textarea class="form-control" name="short_description" rows="8" maxlength="80"><?php echo $item->short_description?></textarea>
            </div>
            <div class="form-group">
                <label for="summernote">Полное описание:</label>
                <textarea class="form-control summernote" name="long_description" rows="8"><?php echo $item->long_description?></textarea>
            </div>
            <div class="form-group">
                <label for="summernote">Характеристики:</label>
                <textarea class="form-control summernote" name="characteristics" rows="8"><?php echo $item->characteristics?></textarea>
            </div>
            <div class="form-group">
                <label for="img">Изображение коллекции</label>
                <?php if(($item->img)){?>
                    <span class="glyphicon glyphicon-ok" style="color: darkgreen"></span><br>
                    <img src="<?php echo '/img/collection/'.$item->img?>" alt="" width="200">
                <?php }?>
                <input type="file" id="img" name="img">
            </div>
            <div class="form-group">
                <label for="metaKeywords">Ключевые слова:</label>
                <input type="text" class="form-control" id="metaKeywords" name="metaKeywords" value="<?php echo $item->metaKeywords?>">
            </div>
            <div class="form-group">
                <label for="metaDescription">Description:</label>
                <input type="text" class="form-control" id="metaDescription" name="metaDescription" value="<?php echo $item->metaDescription?>">
            </div>
            <h2>Позиции</h2>
            <div id="product_list" class="row">
                <?php if (!empty($product)) {
                    foreach ($product as $productItem) {?>
                        <div class="col-md-6 productItem panel panel-default" id="productItem<?php if(!empty($productItem->id)) echo $productItem->id;?>">
                            <div style="float:right;margin-top:14px;cursor:pointer"><span class="glyphicon glyphicon-remove" onclick="Product.removeItem(<?php if(!empty($productItem->id)) echo $productItem->id;?>)"></span></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="product_id">id</label>
                                    <input type="text" class="form-control" id="product_id" name="product_id[]" readonly value="<?php if(!empty($productItem->id)) echo $productItem->id;?>">
                                </div>
                                <div class="form-group">
                                    <label for="product_name">Название</label>
                                    <input type="text" class="form-control" id="product_name" name="product_name[]" value="<?php if(!empty($productItem->name)) echo $productItem->name;?>">
                                </div>
                                <div class="form-group">
                                    <label for="product_width">Ширина</label>
                                    <input type="text" class="form-control" id="product_width" name="product_width[]" value="<?php if(!empty($productItem->width)) echo $productItem->width?>">
                                </div>
                                <div class="form-group">
                                    <label for="product_height">Высота</label>
                                    <input type="text" class="form-control" id="product_height" name="product_height[]" value="<?php if(!empty($productItem->height)) echo $productItem->height?>">
                                </div>
                                <div class="form-group">
                                    <label for="product_unit">Единица измерения</label>
                                    <input type="text" class="form-control" id="product_unit" name="product_unit[]" value="<?php if(!empty($productItem->unit)) echo $productItem->unit?>">
                                </div>
                                <div class="form-group">
                                    <label for="product_cost">Стоимость</label>
                                    <input type="text" class="form-control" id="product_cost" name="product_cost[]" value="<?php if(!empty($productItem->cost)) echo $productItem->cost?>">
                                </div>
                                <div class="form-group">
                                    <label for="product_costOf">Стоимость за...</label>
                                    <input type="text" class="form-control" id="product_costOf" name="product_costOf[]" value="<?php if(!empty($productItem->costOf)) echo $productItem->costOf?>">
                                </div>
                                <div class="form-group" style="overflow: hidden">
                                    <label for="product_img">Изображение продукта</label>
                                    <?php if(($productItem->img)){?>
                                        <span class="glyphicon glyphicon-ok" style="color: darkgreen"></span><br>
                                        <img src="<?php echo '/img/product/'.$productItem->img?>" alt="" height="100">
                                    <?php }?>
                                    <input type="file" id="product_img" name="product_img[]">
                                </div>
                            </div>
                        </div>
                    <?php }
                } ?>
                <div class="col-md-6 productItem panel panel-default" id="addItem1">
                  <div style="float:right;margin-top:14px;cursor:pointer"><span class="glyphicon glyphicon-remove" onclick="$('#addItem1').remove()"></span></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="product_name">Название</label>
                            <input type="text" class="form-control" id="product_name" name="product_name[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_width">Ширина</label>
                            <input type="text" class="form-control" id="product_width" name="product_width[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_height">Высота</label>
                            <input type="text" class="form-control" id="product_height" name="product_height[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_unit">Единица измерения</label>
                            <input type="text" class="form-control" id="product_unit" name="product_unit[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_cost">Стоимость</label>
                            <input type="text" class="form-control" id="product_cost" name="product_cost[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_costOf">Стоимость за...</label>
                            <input type="text" class="form-control" id="product_costOf" name="product_costOf[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_img">Изображение продукта</label>
                            <input type="file" id="product_img" name="product_img[]">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <button type="button" class="btn btn-primary" onclick="Product.addItem()">+ Добавить позицию</button>
            </div>
            <div class="form-group col-sm-12">
                <button type="submit" class="btn btn-success">Сохранить</button>
            </div>

        </form>
    </div>
</div>
