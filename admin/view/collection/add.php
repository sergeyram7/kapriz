<?php
if (!empty($this->data['product'])) {
    $product = $this->data['product'];
}
if(!empty($this->data['save'])){
    echo '<div class="alert alert-success"><strong>Сохранено</strong></div>';
}
?>
<div class="row">
    <div class="col-md-12">
        <h2>Добавление коллекции</h2>
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name">Имя:</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="alias">Алиас (формируется сам, после имени):</label>
                <input type="text" class="form-control" id="alias" name="alias">
            </div>
            <div class="form-group">
                <label for="isRecommended">Рекомендуемая коллекция:</label>
                <p><input name="isRecommended" type="radio" value="1"> Да</p>
                <p><input name="isRecommended" type="radio" value="0"> Нет</p>
            </div>
            <div class="form-group">
                <label for="category">Категория:</label>
                <select class="form-control" id="category" name="category_id">
                    <?php foreach($this->data['category'] as $category){?>
                        <option value="<?php echo $category->id?>"><?php echo $category->name?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="producer">Производитель:</label>
                <select class="form-control" id="producer" name="producer_id">
                    <?php foreach($this->data['producer'] as $producer){?>
                        <option value="<?php echo $producer->id?>"><?php echo $producer->name?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="summernote">Краткое описание:</label>
                <textarea class="form-control" name="short_description" rows="8" maxlength="80"></textarea>
            </div>
            <div class="form-group">
                <label for="summernote">Полное описание:</label>
                <textarea class="form-control summernote" name="long_description" rows="8"></textarea>
            </div>
            <div class="form-group">
                <label for="summernote">Характеристики:</label>
                <textarea class="form-control summernote" name="characteristics" rows="8"></textarea>
            </div>
            <div class="form-group">
                <label for="img">Изображение коллекции</label>
                <input type="file" id="img" name="img">
            </div>
            <div class="form-group">
                <label for="metaKeywords">Ключевые слова:</label>
                <input type="text" class="form-control" id="metaKeywords" name="metaKeywords">
            </div>
            <div class="form-group">
                <label for="metaDescription">Description:</label>
                <input type="text" class="form-control" id="metaDescription" name="metaDescription">
            </div>
            <h2>Позиции</h2>
            <div id="product_list" class="row">
              <div class="col-md-6 productItem panel panel-default" id="addItem1">
                <div style="float:right;margin-top:14px;cursor:pointer"><span class="glyphicon glyphicon-remove" onclick="$('#addItem1').remove()"></span></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="product_name">Название</label>
                            <input type="text" class="form-control" id="product_name" name="product_name[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_width">Ширина</label>
                            <input type="text" class="form-control" id="product_width" name="product_width[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_height">Высота</label>
                            <input type="text" class="form-control" id="product_height" name="product_height[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_unit">Единица измерения</label>
                            <input type="text" class="form-control" id="product_unit" name="product_unit[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_cost">Стоимость</label>
                            <input type="text" class="form-control" id="product_cost" name="product_cost[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_costOf">Стоимость за...</label>
                            <input type="text" class="form-control" id="product_costOf" name="product_costOf[]" value="">
                        </div>
                        <div class="form-group">
                            <label for="product_img">Изображение продукта</label>
                            <input type="file" id="product_img" name="product_img[]">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <button type="button" class="btn btn-primary" onclick="Product.addItem()">+ Добавить позицию</button>
            </div>
            <div class="form-group col-sm-12">
                <button type="submit" class="btn btn-success">Сохранить</button>
            </div>

        </form>
    </div>
</div>
