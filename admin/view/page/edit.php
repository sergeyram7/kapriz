<?php $item = $this->data['page'][0];
if(!empty($this->data['save'])){
    echo '<div class="alert alert-success"><strong>Сохранено</strong></div>';
}
?>
<div class="row">
    <div class="col-md-12">
        <h2>Редактирование страницы <?php echo $item->name?></h2>
        <form role="form" method="post">
            <div class="form-group">
                <label for="name">Имя:</label>
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $item->name?>">
            </div>
            <div class="form-group">
                <label for="alias">Алиас (формируется сам, после имени):</label>
                <input type="text" class="form-control" id="alias" name="alias" value="<?php echo $item->alias?>">
            </div>

            <div class="form-group">
                <label for="summernote">Описание:</label>
                <textarea class="form-control" id="summernote" name="text" rows="8"><?php echo $item->text?></textarea>
            </div>

            <div class="form-group">
                <label for="metaKeywords">Ключевые слова:</label>
                <input type="text" class="form-control" id="metaKeywords" name="metaKeywords" value="<?php echo $item->metaKeywords?>">
            </div>
            <div class="form-group">
                <label for="metaDescription">Description:</label>
                <input type="text" class="form-control" id="metaDescription" name="metaDescription" value="<?php echo $item->metaDescription?>">
            </div>
            <button type="submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
</div>