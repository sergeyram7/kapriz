<? if (!empty($this->data['producerList'])): ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <span style="font-size: 18pt">Производители для данной категории</span>
            <div class="row">
                <?php foreach ($this->data['producerList'] as $producer) { ?>
                    <div class="col-md-2" style="text-align: center">
                        <a href="<?php echo $_SERVER["REQUEST_URI"] . '/' . $producer->alias ?>">
                            <img src="/img/producer/<?php echo $producer->img ?>" alt="<?php echo $producer->name ?>"
                                 title="<?php echo $producer->name ?>" style="width: 100%">
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<? endif ?>

    <div class="row">
        <div class="col-xs-12">
            <h1 class="underline-header"><?php echo $this->data['category'][0]->title ?></h1>
            <? if (!empty($this->data['collection'])): ?>
            <div class="row">
                <?php
                foreach ($this->data['collection'] as $key => $collection) {
                    ?>
                    <div class="col-md-3">
                        <div class="thumbnail">
                            <div class="wrap-img"><a href="/category/view/<?php echo $this->data['category'][0]->alias . '/' . $this->data['producerItem'][$key]->alias . '/' . $collection->id . '/' . $collection->alias ?>"><img
                                    src="/img/collection/min/<?php echo $collection->img ?>"
                                    alt="<?php echo $collection->name ?>"></a></div>
                            <div class="caption">
                                <h3>
                                    <a href="/category/view/<?php echo $this->data['category'][0]->alias . '/' . $this->data['producerItem'][$key]->alias . '/' . $collection->id . '/' . $collection->alias ?>"><?php echo $collection->name ?></a>
                                </h3>
                                <p class="short_description"><?php echo $collection->short_description ?></p>
                                <ul>
                                    <li><strong>Позиций в
                                            коллекции: </strong><?php echo($this->data['countProduct'][$key]) ?></li>
                                </ul>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a href="/producer/view/<?php echo($this->data['producerItem'][$key]->alias)?>" class="btn btn-link btn-producer" role="button" title="Посмотреть все коллекции от '<?php echo($this->data['producerItem'][$key]->name)?>'"><?php echo $this->data['producerItem'][$key]->name;?></a>
                                    </div>
                                    <div class="col-xs-12">
                                        <a href="/category/view/<?php echo $this->data['category'][0]->alias . '/' . $this->data['producerItem'][$key]->alias . '/' . $collection->id . '/' . $collection->alias?>" class="btn btn-danger" role="button">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <? endif ?>
        </div>
    </div>

<div class="row">
    <div class="col-xs-12">
        <?php echo $this->data['category'][0]->description?>
    </div>
</div>