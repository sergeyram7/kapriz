<div class="row">
    <div class="col-md-12">
        <h2>Все производители</h2>
        <a href="/admin/producer/add" class="btn btn-success">+ Добавить</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#id</th>
                <th>Название</th>
                <th>Алиас</th>
                <th>Опции</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($this->data['producer'] as $key => $item){ ?>
                <tr id="item<?php echo $item->id ?>">
                    <td>
                        <?php echo $item->id ?>
                    </td>
                    <td>
                        <?php echo $item->name ?>
                    </td>
                    <td>
                        <?php echo $item->alias ?>
                    </td>
                    <td>
                        <a href="/admin/producer/edit/<?php echo $item->id ?>" class="btn btn-default">Редактировать</a>
                        <button type="button" onclick="Producer.removeItem(<?php echo $item->id ?>)" class="btn btn-danger">Удалить</button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>