function setProd(id,num,cost){
    var cart = document.getElementById('cart');
    var url = '/function/cart.php';
    if(cart.getAttribute('data-display')=='none'){
        //document.getElementById('numCart').innerHTML = num;
        //document.getElementById('costCart').innerHTML = cost;
        cart.setAttribute('data-display','block');
        //cart.style.display = "block";
        $("#cart").fadeIn();
    }
    jQuery.ajax({
        url:     url, //Адрес подгружаемой страницы
        type:     "GET", //Тип запроса
        dataType: "html", //Тип данных
        //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
        data: "id="+id+"&num="+num+"&cost="+cost,
        success: function(response) { //Если все нормально
            var numCart = Number(document.getElementById('numCart').innerHTML);
            var costCart = Number(document.getElementById('costCart').innerHTML);
            document.getElementById('numCart').innerHTML = numCart + num;
            document.getElementById('costCart').innerHTML = costCart + cost;
        },
        error: function(response) { //Если ошибка
            document.getElementById('cart').innerHTML = "Ошибка при отправке формы";
        }
    });
}

var Cart = {
    url: '/function/cart-list.php',
    numChange: function(way,id,cost,key){
        var inputNum = document.getElementById('spinner'+id);
        var totalCost = document.getElementById('total-cost');
        if(way == 'down'){
            if(inputNum.value>1){
                jQuery.ajax({
                    url:     this.url, //Адрес подгружаемой страницы
                    type:     "GET", //Тип запроса
                    dataType: "html", //Тип данных
                    //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
                    data: "action="+'change'+"&way="+'down'+"&key="+key,
                    success: function(response) { //Если все нормально
                        inputNum.value--;
                        totalCost.innerHTML = Number(totalCost.innerHTML)-cost;
                    },
                    error: function(response) { //Если ошибка
                        document.getElementById('cart').innerHTML = "Ошибка при отправке формы";
                    }
                });
            }
        } else if(way == 'up'){
            jQuery.ajax({
                url:     this.url, //Адрес подгружаемой страницы
                type:     "GET", //Тип запроса
                dataType: "html", //Тип данных
                //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
                data: "action="+'change'+"&way="+'up'+"&key="+key,
                success: function(response) { //Если все нормально
                    inputNum.value++;
                    totalCost.innerHTML = Number(totalCost.innerHTML)+cost;
                },
                error: function(response) { //Если ошибка
                    document.getElementById('cart').innerHTML = "Ошибка при отправке формы";
                }
            });
        }
    },
    removeItem: function(id,cost,key){
        var inputNum = document.getElementById('spinner'+id);
        var totalCost = document.getElementById('total-cost');
        var item = document.getElementById('item'+id);
        jQuery.ajax({
            url:     this.url, //Адрес подгружаемой страницы
            type:     "GET", //Тип запроса
            dataType: "html", //Тип данных
            //data: jQuery("#id"+id+"#num"+num+"#cost"+cost).serialize(),
            data: "action="+'remove'+"&key="+key+"&totalCost="+(Number(totalCost.innerHTML)-(cost*inputNum.value)),
            success: function(response) { //Если все нормально
                totalCost.innerHTML = Number(totalCost.innerHTML)-(cost*inputNum.value);
                $('#item'+id).remove();
            },
            error: function(response) { //Если ошибка
                document.getElementById('cart').innerHTML = "Ошибка при отправке формы";
            }
        });
    },
    send: function(){
        var inputName = document.getElementById('inputName').value;
        var inputPhone = document.getElementById('inputPhone').value;
        var inputEmail = document.getElementById('inputEmail').value;
        var inputAddress = document.getElementById('inputAddress').value;
        var checkPayment = $('input[name=checkPayment]:checked').val();
        var data = "action="+'orders'+"&userName="+inputName+"&phone="+inputPhone+"&email="+inputEmail+"&address="+inputAddress+"&payMethod="+checkPayment;
        jQuery.ajax({
            url:     '/function/send.php', //Адрес подгружаемой страницы
            type:     "GET", //Тип запроса
            dataType: "html", //Тип данных
            data: data,
            success: function(response) { //Если все нормально
                $('#cart-list').empty();
                var goodText = '<div class="alert alert-success"><p><strong>Спасибо!</strong> Ваш заказ оформлен, вскоре мы с вами свяжемся! Запишите номер заказа: <strong>'+response+'</strong></p></div>';
                $('#cart-list').html(goodText);
            },
            error: function(response) { //Если ошибка
                alert('error');
            }
        });
    }
};