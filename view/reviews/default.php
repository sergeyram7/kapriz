<div class="row">
    <div class="col-xs-12">
        <h2 class="underline-header">Отзывы</h2>
        <div class="row">
        <?php
        foreach($this->data['reviews'] as $review){?>
            <div class="media" style="border-bottom: 1px dashed #e95d39; padding-bottom: 15px">
                <div class="pull-left">
                    <img class="media-object img-circle" src="<?php echo isset($review->img)?'/img/review/'.$review->id . '/' . $review->img:'/img/review/no-avatar.png'?>" alt="..." width="100">
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo $review->userName?> <?php echo $review->date?></h4>
                    <p><?php echo $review->text?></p>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h2 class="underline-header">Оставить отзыв</h2>
        <div class="row">
            <div class="form col-xs-6">
                <div id="alert-review" class="alert" style="display: none"></div>
                <div class="form-group">
                    <label for="reviewName">Ваше имя</label>
                    <input type="text" class="form-control" id="reviewName" placeholder="Иван">
                </div>
                <div class="form-group">
                    <label for="reviewText" class="control-label">Отзыв</label>
                    <textarea class="form-control" id="reviewText" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default" onclick="Reviews.send()">Отправить</button>
                </div>
            </div>
        </div>
    </div>
</div>