<?php
    $page = $this->data['page'][0];
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="underline-header"><?php echo $page->name?></h1>
        <?php echo $page->text?>
    </div>
</div>
