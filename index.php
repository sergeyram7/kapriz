<?php
    header('Content-Type: text/html; charset=utf-8');

    session_start();

//    ini_set("display_errors",1);
//    error_reporting(E_ALL);

    require_once __DIR__ . '/autoload.php';

    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $pathParts = explode('/', $path);

    //$city = !empty($pathParts[1]) ? ucfirst($pathParts[1]):'all';


    //$ctrl = (!empty($pathParts[1])||!isset($pathParts[1])) ? 'App\Controller\\'.ucfirst($pathParts[1]):'App\Controller\Index';
if(!empty($pathParts[1])){
  if($pathParts[1]=='index.php'){
    //var_dump($pathParts[1]);
    $ctrl = "App\Controller\Index";
  } else {
    $ctrl = 'App\Controller\\'.ucfirst($pathParts[1]);
  }
} else {
    $ctrl = 'App\Controller\Index';
}
    //var_dump($ctrl);
    if(!empty($pathParts[2])){
        $action = 'action_'.ucfirst($pathParts[2]);
    } else {
        $action = 'action_all';
    }
    //$action = (!empty($pathParts[2])||!isset($pathParts[2])) ? 'action_'.ucfirst($pathParts[2]):'action_all';
    //if(isset($_GET['id'])) $action = 'action_view';
    //var_dump($action);

    $controller=new $ctrl;

    if(method_exists($controller,$action)){
        $controller->$action();
    } else {
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        include_once __DIR__ .'/404.html';
    }


    //var_dump($_SESSION);
?>
