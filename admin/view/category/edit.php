<?php $category = $this->data['category'][0];
if(!empty($this->data['save'])){
    echo '<div class="alert alert-success"><strong>Сохранено</strong></div>';
}
?>
<div class="row">
    <div class="col-md-12">
        <h2>Редактирование категории <?php echo $category->name?></h2>
        <form role="form" method="post">
            <div class="form-group">
                <label for="name">Имя:</label>
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $category->name?>">
            </div>
            <div class="form-group">
                <label for="title">Заголовок страницы:</label>
                <input type="text" class="form-control" id="title" name="title" value="<?php echo $category->title?>">
            </div>
            <div class="form-group">
                <label for="alias">Алиас (формируется сам, после имени):</label>
                <input type="text" class="form-control" id="alias" name="alias" value="<?php echo $category->alias?>">
            </div>

            <div class="form-group">
                <label for="summernote">Описание:</label>
                <textarea class="form-control" id="summernote" name="description" rows="8"><?php echo $category->description?></textarea>
            </div>

            <div class="form-group">
                <label for="metaKeywords">Ключевые слова:</label>
                <input type="text" class="form-control" id="metaKeywords" name="metaKeywords" value="<?php echo $category->metaKeywords?>">
            </div>
            <div class="form-group">
                <label for="metaDescription">Description:</label>
                <input type="text" class="form-control" id="metaDescription" name="metaDescription" value="<?php echo $category->metaDescription?>">
            </div>
            <button type="submit" class="btn btn-success">Сохранить</button>
        </form>
    </div>
</div>