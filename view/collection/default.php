<?php $collection = $this->data['collection'][0]?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="underline-header">
            <?php if($collection->name != 'Сопутствующие товары'){?>
            Коллекция "<?php echo $collection->name?>"
            <?php
                } else {
                    echo $collection->name;
                }
            ?>
        </h1>
        <div class="row">
            <div class="col-md-5">
                <a onclick="imgCollection.open('/img/collection/<?php echo $collection->img?>')" href="#" data-toggle="modal" data-target="#modalBox"><img onclick="imgCollection.open('/img/collection/<?php echo $collection->img?>')" src="/img/collection/<?php echo $collection->img?>" class="img-thumbnail" alt="Коллекция '<?php echo $collection->name?>'" title="Коллекция '<?php echo $collection->name?>'" style="width: 100%"></a>
                <div style="padding-left: 15px">
                    <?php if(!empty($collection->characteristics)){?>
                    <h3 style="text-transform: uppercase">Характеристики плитки</h3>
                        <?php echo $collection->characteristics?>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-7">
                <h2 style="margin-top: 0">Описание</h2>
                    <?php echo $collection->long_description?>
                <h2>Позиции</h2>
                <?php
                foreach($this->data['product'] as $key => $product){?>
                    <div class="media" style="padding-bottom:15px;border-bottom: 1px solid #ccc">
                        <div class="pull-left">
                            <a onclick="imgCollection.open('/img/product/<?php echo $product->img?>')" href="#" data-toggle="modal" data-target="#modalBox"><img class="media-object" src="/img/product/<?php echo $product->img?>" alt="<?php echo $product->name?>" width="200"></a>
                        </div>
                        <div class="media-body">
                            <small><?php echo $this->data['collection'][0]->name?></small>
                            <h4 class="media-heading"><?php echo $product->name?></h4>
                            <p style="color: darkgreen"><?php echo $product->cost?> руб. / <?php echo $product->costOf?></p>
                            <button type="button" class="btn btn-danger" onclick="setProd(<?php echo $product->id ?>,1,<?php echo $product->cost?>)">+ Добавить в корзину</button>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>